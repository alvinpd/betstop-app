/*
 * File:   main.c
 * Author: difuntoruma
 *
 * Created on November 8, 2012, 8:56 AM
 */

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <sys/stat.h>

#include <libxml/tree.h>
#include <libxml/parser.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>
#include <libxml/HTMLparser.h>
#include <libxml/HTMLtree.h>

#define BS_EINVAL		1
#define BS_ENOMEM		2
#define BS_ENOENT		3

#define bs_info(fmt, ...)	fprintf(stdout, fmt, ##__VA_ARGS__)
#define bs_err(fmt, ...)	fprintf(stderr, fmt, ##__VA_ARGS__)

struct match {
	unsigned long id;
	time_t	sched;
	double  odd1;
	double  odd2;
	double  odd3;
};

struct bookie {
	struct match match;
};

struct betfair_parser {
	struct tm     tm;
	struct match  *m;
	struct bookie *bk;
};

static void bf_str_trim(const char *in, char *out)
{
        char *start, *end;

        start = (char *)in;

        while (*start++ < 0x21);
        start--;

        end = (char*)(start + strlen(start));
        while (*end-- < 0x21);

        memcpy(out, start, end - start + 2);
}


xmlXPathObjectPtr
betstop_nodeset(xmlDocPtr doc, xmlChar *xpath)
{
        xmlXPathContextPtr context;
        xmlXPathObjectPtr  result;

        context = xmlXPathNewContext(doc);
        if (context == NULL) {
                bs_err("Error in xmlXPathNewContext\n");
                return NULL;
        }

        result = xmlXPathEvalExpression(xpath, context);
        xmlXPathFreeContext(context);
        if (result == NULL) {
                bs_err("Error in xmlXPathEvalExpression\n");
                return NULL;
        }

        if(xmlXPathNodeSetIsEmpty(result->nodesetval)){
                xmlXPathFreeObject(result);
                bs_err("No result\n");
                return NULL;
        }

        return result;
}

static void bf_parse_date(struct betfair_parser *bp, xmlDocPtr doc, xmlNodePtr cur)
{
	char    date[64];
	xmlChar *v;

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "tr"))
			break;
		cur = cur->next;
	}

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "td"))
			break;
		cur = cur->next;
	}

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "h3"))
			break;
		cur = cur->next;
	}

	v = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
	if (v) {
		memset(&bp->tm, 0, sizeof(bp->tm));
		memset(date, 0, 64);
		bf_str_trim(v, date);
		strptime(date, "%a, %d %b %Y", &bp->tm);

		xmlFree(v);
	}
}

static void bf_parse_teams(struct betfair_parser *bp, xmlDocPtr doc, xmlNodePtr cur)
{
	char team[32];
	xmlChar *c, *v;
	xmlNodePtr tmp;

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "tr"))
			break;
		cur = cur->next;
	}

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "td"))
			break;
		cur = cur->next;
	}

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "a")) {
			tmp = cur->xmlChildrenNode;
			while (tmp != NULL) {
				c = NULL;
				if (!xmlStrcmp(tmp->name, (const xmlChar *) "span"))
					c = xmlGetProp(tmp,
						(const xmlChar *) "class");

				memset(team, 0, 32);
				if (c && xmlStrstr(c, (const xmlChar *) "home")) {
					v = xmlNodeListGetString(doc,
						tmp->xmlChildrenNode, 1);
					if (v) {
						bf_str_trim(v, team);
						printf("Team: 1. %s - ", team);
						xmlFree(v);
					}
				} else if (c && xmlStrstr(c, (const xmlChar *) "away")) {
					v = xmlNodeListGetString(doc,
						tmp->xmlChildrenNode, 1);
					if (v) {
						bf_str_trim(v, team);
						printf("2. %s\n", team);
						xmlFree(v);
					}
				}

				if (c)
					xmlFree(c);
				tmp = tmp->next;
			}
		} else if (!xmlStrcmp(cur->name, (const xmlChar *) "span")) {
			tmp = cur->xmlChildrenNode;
			while (tmp != NULL) {
				v = NULL;
				if (!xmlStrcmp(tmp->name, (const xmlChar *) "span"))
					v = xmlNodeListGetString(doc,
						tmp->xmlChildrenNode, 1);

				if (v) {
					strptime(v, "%H:%M", &bp->tm);
					xmlFree(v);
				}
				tmp = tmp->next;
			}
		}

		cur = cur->next;
	}
}

static void bf_parse_odds(struct betfair_parser *bp, xmlDocPtr doc, xmlNodePtr cur)
{
	char odd[10];
	xmlChar *c, *v;
	xmlNodePtr tmp;
	struct match *m = bp->m;

	m->odd1 = 0;
	m->odd2 = 0;
	m->odd3 = 0;

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "tr"))
			break;
		cur = cur->next;
	}

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		c = NULL;
		if (!xmlStrcmp(cur->name, (const xmlChar *) "td"))
			c = xmlGetProp(cur, (const xmlChar *) "class");

		if (c && xmlStrstr(c, "odds back")) {
			tmp = cur->xmlChildrenNode;
			while (tmp != NULL) {
				if (!xmlStrcmp(tmp->name,
						(const xmlChar *) "button"))
					break;
				tmp = tmp->next;
			}

			tmp = tmp->xmlChildrenNode;
			while (tmp != NULL) {
				if (!xmlStrcmp(tmp->name,
						(const xmlChar *) "span"))
					break;
				tmp = tmp->next;
			}

			v = xmlNodeListGetString(doc, tmp->xmlChildrenNode, 1);
			if (v) {
				bf_str_trim(v, odd);
				if (m->odd1 == 0)
					m->odd1 = strtod(odd, NULL);
				else if (m->odd2 == 0)
					m->odd2 = strtod(odd, NULL);
				else if (m->odd3 == 0)
					m->odd3 = strtod(odd, NULL);

				xmlFree(v);
			}
		}

		if (c)
			xmlFree(c);
		cur = cur->next;
	}
}

static void bf_parse_match(struct betfair_parser *bp, xmlDocPtr doc, xmlNodePtr cur)
{
	xmlChar *p;
	struct match *m = bp->m;

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "tr"))
			break;
		cur = cur->next;
	}

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "td"))
			break;
		cur = cur->next;
	}

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		p = NULL;
		if (!xmlStrcmp(cur->name, (const xmlChar *) "table"))
			p = xmlGetProp(cur, (const xmlChar *) "class");

		if (p && xmlStrstr(p, (const xmlChar *) "left"))
			bf_parse_teams(bp, doc, cur);
		else if (p && xmlStrstr(p, (const xmlChar *) "right"))
			bf_parse_odds(bp, doc, cur);

		if (p)
			xmlFree(p);
		cur = cur->next;
	}

	m->sched = mktime(&bp->tm);
	printf("Odds: %.02f %.02f %.02f\n", m->odd1, m->odd2, m->odd3);
	printf("Date: %s\n", ctime(&m->sched));

}

static void bf_parse_entry(struct betfair_parser *bp, xmlDocPtr doc, xmlNodePtr cur)
{
	xmlChar *p = xmlGetProp(cur, (const xmlChar *) "id");

	if (p == NULL)
		return;

	if (xmlStrstr(p, (const xmlChar *) "-coming-up-heading") != NULL)
		bf_parse_date(bp, doc, cur);
	else if (xmlStrstr(p, (const xmlChar *) "-market") != NULL)
		bf_parse_match(bp, doc, cur);

	if (p)
		xmlFree(p);
}

static int betfair_parse(struct bookie *bk, const char *fname)
{
        int          fd, ret;
        char         *htmlbuff;
        struct stat  st;

        htmlDocPtr        doc;
        xmlNodeSetPtr     nodeset;
        xmlXPathObjectPtr nodes;
        htmlParserCtxtPtr parser;

        bs_info("Parsing bookie file: %s\n", fname);

        ret = stat(fname, &st);
        if (ret != 0) {
                bs_err("File not found: %s\n", fname);
                return BS_EINVAL;
        }

        htmlbuff = malloc(st.st_size);
        if (!htmlbuff) {
                bs_err("Unable to allocate %ld bytes\n", st.st_size);
                return -BS_ENOMEM;
        }

        fd = open(fname, O_RDONLY);
        if (fd < 0) {
                ret = -BS_ENOENT;
                goto fail_open;
        }

        parser = htmlCreateMemoryParserCtxt(htmlbuff, st.st_size);
        if (!parser) {
                bs_err("Failed to create an html parser context\n");
                ret = -BS_ENOMEM;
                goto fail_ctxt;
        }

        htmlCtxtUseOptions(parser, HTML_PARSE_NOBLANKS | HTML_PARSE_NOERROR |
                                   HTML_PARSE_NOWARNING | HTML_PARSE_NONET);

        doc = htmlCtxtReadFd(parser, fd, NULL, NULL, 0);
        if (!doc) {
                bs_err("Failed to open the html document\n");
                ret = -BS_EINVAL;
                goto fail_readfd;
        }

        nodes = betstop_nodeset(doc, (xmlChar*) "//table/tbody");
        if (nodes) {
                int    i;
                struct betfair_parser bp;

                bp.m = &bk->match;
                bp.bk = bk;

                nodeset = nodes->nodesetval;
                for (i = 0; i < nodeset->nodeNr; i++)
                        bf_parse_entry(&bp, doc, nodeset->nodeTab[i]);

                xmlXPathFreeObject(nodes);
        }

        xmlFreeDoc(doc);

fail_readfd:
        htmlFreeParserCtxt(parser);

fail_ctxt:
        close(fd);

fail_open:
        free(htmlbuff);

        return 0;

}

/*
 *
 */
int main(int argc, char** argv)
{
	struct bookie bk;

	xmlInitParser();
        LIBXML_TEST_VERSION

	betfair_parse(&bk, "bf.dat");

	xmlMemoryDump();
        xmlCleanupParser();

	return (EXIT_SUCCESS);
}

/* End of file */
