/*
 * File:   match.c
 * Author: Alvin Difuntorum <alvinpd09@gmail.com>
 *
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

#include "betstop.h"
#include "bookie.h"

struct bookies_leagues {
	struct list_head list;

	char url[512];
	unsigned long bookie_id;
	unsigned long league_id;
};

static int cbk_query_bl(void *list, int argc, char **argv, char **col_name)
{
	int    i;
	struct list_head *lh = list;
	struct bookies_leagues *bl = NULL;

	/* Allocate a bookie_list item */
	bl = malloc(sizeof(*bl));
	if (!bl)
		return SQLITE_NOMEM;

	INIT_LIST_HEAD(&bl->list);
	for (i = 0; i < argc; i++) {
		if (!strcmp(col_name[i], "url"))
			snprintf(bl->url, ARRAY_SIZE(bl->url), "%s", argv[i]);
		else if (!strcmp(col_name[i], "bookie_id"))
			bl->bookie_id = strtoul(argv[i], NULL, 10);
		else if (!strcmp(col_name[i], "league_id"))
			bl->league_id = strtoul(argv[i], NULL, 10);
	}

	list_add_tail(&bl->list, lh);

	return 0;
}

static int cbk_query_teams(void *list, int argc, char **argv, char **col_name)
{
	int    i;
	struct team *t = NULL;
	struct list_head *lh = list;

	/* Allocate a bookie_list item */
	t = malloc(sizeof(*t));
	if (!t)
		return SQLITE_NOMEM;

	INIT_LIST_HEAD(&t->list);
	for (i = 0; i < argc; i++) {
		if (!strcmp(col_name[i], "team_id"))
			t->id = strtoul(argv[i], NULL, 10);
		else if (!strcmp(col_name[i], "name"))
			snprintf(t->name, ARRAY_SIZE(t->name), "%s", argv[i]);

	}

	list_add_tail(&t->list, lh);

	return 0;
}

static int bm_match_load(struct betstop *bs, struct bookie *bk,
			 struct bookies_leagues *bl, const char *sport, int idx)
{
	int    ret = 0;
	char   q[1024], fname[255], *err_msg;
	time_t tmt = time(NULL);
	struct tm tm;
	struct team *p, *r;

	/* Abort if fetch or parse functions are not defined */
	if (bk->fetch == NULL || bk->parse == NULL) {
		bs_err("%s undefined fetch or parse functions", bk->slug);
		return -BS_EINVAL;
	}

	memcpy(&tm, localtime(&tmt), sizeof(tm));

	snprintf(fname, ARRAY_SIZE(fname), "%s/%s_%s_%d%02d%02d_%d.dat",
		BS_DATA_CACHE, sport, bk->slug, tm.tm_year + 1900,
		tm.tm_mon + 1, tm.tm_mday, idx);
	ret = bk->fetch(bk, bl->url, fname);

	if (ret == 0) {
		INIT_LIST_HEAD(&bk->teams);
		bk->match.bookie = bl->bookie_id;
		bk->match.league = bl->league_id;

		sqlite3_snprintf(ARRAY_SIZE(q), q,
			"SELECT `team_id`, `name` FROM `bookies_teams` " \
			"WHERE `bookie_id`=%ld AND `league_id`=%ld",
			bk->match.bookie, bk->match.league);

		pthread_mutex_lock(&bs->dbmtx);
		ret = sqlite3_exec(bs->db, q, cbk_query_teams,
				   &bk->teams, &err_msg);
		pthread_mutex_unlock(&bs->dbmtx);
		if (ret != SQLITE_OK) {
			bs_err("Query failed: %s\nQuery:\n%s\n", err_msg, q);
			sqlite3_free(err_msg);
			return ret;
		}

		ret = bk->parse(bk, fname);

		list_for_each_entry_safe(p, r, &bk->teams, list) {
			list_del(&p->list);
			free(p);
		}
	}

	return ret;
}

void *run_match_thread(void *arg)
{
	int    i, ret;
	char   q[1024];
	char   *err_msg;
	struct bookie *bk = arg;
	struct betstop *bs = bk->bs;
	struct bookies_leagues *bl, *l;
	struct list_head bl_list;

	/* Get the urls of this bookie for this sport */
	printf("Starting thread %s with id=%ld\n", bk->slug, (u64) bk->tid);

	snprintf(q, ARRAY_SIZE(q),
		"SELECT `bookies_leagues`.`url` as `url`," \
		"       `bookies_leagues`.`bookie_id` as `bookie_id`," \
		"       `bookies_leagues`.`league_id` as `league_id`" \
		"    FROM `bookies_leagues` " \
		"INNER JOIN `bookies` " \
		"    ON `bookies_leagues`.`bookie_id`=`bookies`.`id` " \
		"INNER JOIN `sports`" \
		"    ON `bookies_leagues`.`sport_id`=`sports`.`id` " \
		"WHERE `bookies`.`name`='%s' AND `sports`.`slug`='%s'",
		bk->slug, bk->sport);

	INIT_LIST_HEAD(&bl_list);
	pthread_mutex_lock(&bs->dbmtx);
	ret = sqlite3_exec(bs->db, q, cbk_query_bl, &bl_list, &err_msg);
	pthread_mutex_unlock(&bs->dbmtx);
	if (ret != SQLITE_OK) {
		bs_err("Query failed: %s\nQuery:\n%s\n", err_msg, q);
		sqlite3_free(err_msg);
		return NULL;
	}

	i = 0;
	list_for_each_entry_safe(bl, l, &bl_list, list) {
		bm_match_load(bs, bk, bl, bk->sport, i++);

		/* Free the allocated bookie_list item */
		list_del(&bl->list);
		free(bl);
	}

	return NULL;
}

int betstop_match_load(struct betstop *bs, const char *sport)
{
	int ret;
	struct bookie *bk, *k;
	pthread_attr_t attr;

	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

	list_for_each_entry_safe(bk, k, &bs->bookies, list) {
		snprintf(bk->sport, ARRAY_SIZE(bk->sport), "%s", sport);

		ret = pthread_create(&bk->tid, 0, run_match_thread, bk);
		if (ret != 0) {
			bs_err("Unable to create thread: %d bookie %s\n",
				ret, bk->slug);
			continue;
		}
	}

	pthread_attr_destroy(&attr);
	list_for_each_entry_safe(bk, k, &bs->bookies, list) {
		pthread_join(bk->tid, NULL);
	}
	return 0;
}

int betstop_match_add(struct betstop *bs, struct match *m)
{
	int  ret;
	char q[1024], *err_msg;
	unsigned long match_id = 0;

	if (m->team1 == 0 || m->team2 == 0)
		return -BS_EINVAL;

	sqlite3_snprintf(ARRAY_SIZE(q), q,
		"SELECT id FROM `matches` WHERE `bookie_id`=%ld AND " \
		"`league_id`=%ld AND `team1_id`=%ld AND " \
		"`team2_id`=%ld AND `sched`=%ld",
		m->bookie, m->league, m->team1, m->team2, m->sched);
	ret = betstop_db_query_id(bs, q, &match_id);
	if (ret != SQLITE_OK)
		return ret;

	if (match_id == 0)
		sqlite3_snprintf(ARRAY_SIZE(q), q,
			"INSERT INTO `matches`(`bookie_id`, `league_id`," \
			"    `team1_id`, `team2_id`, `sched`, " \
			"    `odd1`, `odd2`, `odd3`) "\
			"VALUES (%ld, %ld, %ld, %ld, %ld, %.02f, %.02f, %.02f)",
			m->bookie, m->league, m->team1, m->team2, m->sched,
			m->odd1, m->odd2, m->odd3);
	else
		sqlite3_snprintf(ARRAY_SIZE(q), q,
			"UPDATE `matches` SET `odd1`=%02f, `odd2`=%02f, `odd3`=%02f "\
			"WHERE `id`=%ld", m->odd1, m->odd2, m->odd3, match_id);

	pthread_mutex_lock(&bs->dbmtx);
	ret = sqlite3_exec(bs->db, q, NULL, 0, &err_msg);
	pthread_mutex_unlock(&bs->dbmtx);
	if (ret != SQLITE_OK) {
		bs_err("Insert query failed: %s\nQuery:\n%s\n", err_msg, q);
		sqlite3_free(err_msg);
	}

	return 0;
}

/* End of file */
