/*
 * File:   betstop.c
 * Author: Alvin Difuntorum <alvinpd09@gmail.com>
 *
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "betstop.h"

int  betstop_init(struct betstop *bs)
{
	int ret = BS_ENONE;
	char fname[255];
	time_t tmt = time(NULL);
	struct tm tm;

	INIT_LIST_HEAD(&bs->bookies);
	INIT_LIST_HEAD(&bs->competitions);
	pthread_mutex_init(&bs->dbmtx, NULL);

	memcpy(&tm, localtime(&tmt), sizeof(tm));
	snprintf(fname, ARRAY_SIZE(fname), "%s/betstop_run_%d%02d%02d.dat",
		BS_DATA_CACHE, tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday);

	bs->ferr = fopen(fname, "wb");
	if (bs->ferr == NULL) {
		bs_err("Unable to open error log file: %s\n", fname);
		return -BS_ENOENT;
	}

	snprintf(bs->dbpath, BS_MAX_PATH, "%s/betstop.db", BS_DATA_CACHE);
	ret = sqlite3_open(bs->dbpath, &bs->db);
	if (ret != SQLITE_OK) {
		bs_err("Unable to initialize database %s: %s\n",
			bs->dbpath, sqlite3_errmsg(bs->db));
		fclose(bs->ferr);
		ret = -BS_ENOENT;
	}

	return ret;
}

void betstop_finalize(struct betstop *bs)
{
	struct odd    *odd, *o;
	struct bookie *bk, *b;
	struct arbitrage *arb, *a;
	struct competition *comp, *c;

	/* Free all bookies */
	list_for_each_entry_safe(bk, b, &bs->bookies, list) {
		bs_dbg("Feeing %s bookie\n", bk->slug);

		list_del(&bk->list);
		/* free(bk); */
	}

	list_for_each_entry_safe(comp, c, &bs->competitions, list) {
		if (comp->arbs)
			free(comp->arbs);

		list_for_each_entry_safe(odd, o, &comp->odds, list) {
			/* Free odds */
			list_del(&odd->list);
			free(odd);
		}

		/* Free arb */
		list_del(&comp->list);
		free(comp);
	}

	if (bs->ferr)
		fclose(bs->ferr);

	if (bs->db)
		sqlite3_close(bs->db);

	pthread_mutex_destroy(&bs->dbmtx);
}

size_t betstop_write(void *buffer, size_t size, size_t nmemb, void *userp)
{
	size_t realsize = size * nmemb;
	struct receive *rcv = userp;

	if (rcv->fd > 0)
		rcv->size += write(rcv->fd, buffer, realsize);

	return realsize;
}

xmlXPathObjectPtr
betstop_nodeset(xmlDocPtr doc, xmlChar *xpath)
{
	xmlXPathContextPtr context;
	xmlXPathObjectPtr  result;

	context = xmlXPathNewContext(doc);
	if (context == NULL) {
		bs_err("Error in xmlXPathNewContext\n");
		return NULL;
	}

	result = xmlXPathEvalExpression(xpath, context);
	xmlXPathFreeContext(context);
	if (result == NULL) {
		bs_err("Error in xmlXPathEvalExpression\n");
		return NULL;
	}

	if(xmlXPathNodeSetIsEmpty(result->nodesetval)){
		xmlXPathFreeObject(result);
		bs_err("No result\n");
		return NULL;
	}

	return result;
}

void betstop_str_trim(const char *in, char *out)
{
        char *start, *end;

        start = (char *)in;

        while (*start++ < 0x21);
        start--;

        end = (char*)(start + strlen(start));
        while (*end-- < 0x21);

        memcpy(out, start, end - start + 2);
}

/* End of file */
