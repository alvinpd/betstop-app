#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-MacOSX
CND_DLIB_EXT=dylib
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/bookie/betinternet.o \
	${OBJECTDIR}/match.o \
	${OBJECTDIR}/bookie/betfred.o \
	${OBJECTDIR}/database.o \
	${OBJECTDIR}/bookie/interwetten.o \
	${OBJECTDIR}/betstop.o \
	${OBJECTDIR}/arbitrage.o \
	${OBJECTDIR}/loader/loader.o \
	${OBJECTDIR}/bookie.o \
	${OBJECTDIR}/bookie/betboo.o \
	${OBJECTDIR}/bookie/gamebookers.o \
	${OBJECTDIR}/bookie/unibet.o \
	${OBJECTDIR}/bookie/mybet.o \
	${OBJECTDIR}/main.o \
	${OBJECTDIR}/bookie/bet-at-home.o \
	${OBJECTDIR}/bookie/bet770.o \
	${OBJECTDIR}/bookie/betfair.o \
	${OBJECTDIR}/bookie/bwin.o


# C Compiler Flags
CFLAGS=`xml2-config --cflags` 

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/betstop-app

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/betstop-app: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.c} `xml2-config --libs` -lcurl -lsqlite3 -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/betstop-app  ${OBJECTFILES} ${LDLIBSOPTIONS} 

${OBJECTDIR}/bookie/betinternet.o: bookie/betinternet.c 
	${MKDIR} -p ${OBJECTDIR}/bookie
	${RM} $@.d
	$(COMPILE.c) -g -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/bookie/betinternet.o bookie/betinternet.c

${OBJECTDIR}/match.o: match.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.c) -g -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/match.o match.c

${OBJECTDIR}/bookie/betfred.o: bookie/betfred.c 
	${MKDIR} -p ${OBJECTDIR}/bookie
	${RM} $@.d
	$(COMPILE.c) -g -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/bookie/betfred.o bookie/betfred.c

${OBJECTDIR}/database.o: database.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.c) -g -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/database.o database.c

${OBJECTDIR}/bookie/interwetten.o: bookie/interwetten.c 
	${MKDIR} -p ${OBJECTDIR}/bookie
	${RM} $@.d
	$(COMPILE.c) -g -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/bookie/interwetten.o bookie/interwetten.c

${OBJECTDIR}/betstop.o: betstop.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.c) -g -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/betstop.o betstop.c

${OBJECTDIR}/arbitrage.o: arbitrage.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.c) -g -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/arbitrage.o arbitrage.c

${OBJECTDIR}/loader/loader.o: loader/loader.c 
	${MKDIR} -p ${OBJECTDIR}/loader
	${RM} $@.d
	$(COMPILE.c) -g -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/loader/loader.o loader/loader.c

${OBJECTDIR}/bookie.o: bookie.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.c) -g -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/bookie.o bookie.c

${OBJECTDIR}/bookie/betboo.o: bookie/betboo.c 
	${MKDIR} -p ${OBJECTDIR}/bookie
	${RM} $@.d
	$(COMPILE.c) -g -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/bookie/betboo.o bookie/betboo.c

${OBJECTDIR}/bookie/gamebookers.o: bookie/gamebookers.c 
	${MKDIR} -p ${OBJECTDIR}/bookie
	${RM} $@.d
	$(COMPILE.c) -g -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/bookie/gamebookers.o bookie/gamebookers.c

${OBJECTDIR}/bookie/unibet.o: bookie/unibet.c 
	${MKDIR} -p ${OBJECTDIR}/bookie
	${RM} $@.d
	$(COMPILE.c) -g -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/bookie/unibet.o bookie/unibet.c

${OBJECTDIR}/bookie/mybet.o: bookie/mybet.c 
	${MKDIR} -p ${OBJECTDIR}/bookie
	${RM} $@.d
	$(COMPILE.c) -g -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/bookie/mybet.o bookie/mybet.c

${OBJECTDIR}/main.o: main.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.c) -g -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/main.o main.c

${OBJECTDIR}/bookie/bet-at-home.o: bookie/bet-at-home.c 
	${MKDIR} -p ${OBJECTDIR}/bookie
	${RM} $@.d
	$(COMPILE.c) -g -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/bookie/bet-at-home.o bookie/bet-at-home.c

${OBJECTDIR}/bookie/bet770.o: bookie/bet770.c 
	${MKDIR} -p ${OBJECTDIR}/bookie
	${RM} $@.d
	$(COMPILE.c) -g -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/bookie/bet770.o bookie/bet770.c

${OBJECTDIR}/bookie/betfair.o: bookie/betfair.c 
	${MKDIR} -p ${OBJECTDIR}/bookie
	${RM} $@.d
	$(COMPILE.c) -g -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/bookie/betfair.o bookie/betfair.c

${OBJECTDIR}/bookie/bwin.o: bookie/bwin.c 
	${MKDIR} -p ${OBJECTDIR}/bookie
	${RM} $@.d
	$(COMPILE.c) -g -Iinclude -MMD -MP -MF $@.d -o ${OBJECTDIR}/bookie/bwin.o bookie/bwin.c

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/betstop-app

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
