/*
 * File:   database.c
 * Author: Alvin Difuntorum <alvinpd09@gmail.com>
 *
 *
 */

#include <stdio.h>
#include <stdlib.h>

#include "betstop.h"

static int cbk_query_id(void *id, int argc, char **argv, char **azColName)
{
	if (argc > 0 && !strcmp(azColName[0], "id"))
		*((unsigned long *) id) = strtoul(argv[0], NULL, 10);

	return 0;
}

int betstop_db_query_id(struct betstop *bs, const char *query, unsigned long *id)
{
	int ret;
	char *err_msg;

	pthread_mutex_lock(&bs->dbmtx);
	ret = sqlite3_exec(bs->db, query, cbk_query_id, id, &err_msg);
	pthread_mutex_unlock(&bs->dbmtx);
	if (ret != SQLITE_OK) {
		bs_err("Insert query failed: %s\nQuery:\n%s\n",
			err_msg, query);
		sqlite3_free(err_msg);
	}

	return ret;
}

/* End of file */
