/*
 * File:   main.c
 * Author: Alvin Difuntorum <alvinpd09@gmail.com>
 *
 * BetStop application entry point
 */

#include <stdio.h>
#include <stdlib.h>

#include "betstop.h"

extern struct bookie bet_at_home;
extern struct bookie bet770;
extern struct bookie betboo;
extern struct bookie betfair;
extern struct bookie betfred;
extern struct bookie betinternet;
extern struct bookie bwin;
extern struct bookie gamebookers;
extern struct bookie interwetten;
extern struct bookie mybet;
extern struct bookie unibet;

struct bk {
	const char    *name;
	struct bookie *bk;
} bookies[] = {
	{
		.bk = &bet_at_home,
		.name = "bet-at-home",
	},
	{
		.bk = &bet770,
		.name = "bet770",
	},
	{
		.bk = &betboo,
		.name = "betboo",
	},
	{
		.bk = &betfair,
		.name = "betfair",
	},
	{
		.bk = &betfred,
		.name = "betfred",
	},
	{
		.bk = &betinternet,
		.name = "betinternet",
	},
	{
		.bk = &bwin,
		.name = "bwin",
	},
	{
		.bk = &gamebookers,
		.name = "gamebookers",
	},
	{
		.bk = &interwetten,
		.name = "interwetten",
	},
	{
		.bk = &mybet,
		.name = "mybet",
	},
	{
		.bk = &unibet,
		.name = "unibet",
	},
};

/*
 *
 */
int main(int argc, char** argv)
{
	int    i, len, ret;
	struct bookie  *bk;
	struct betstop bs;

	printf("-- Starting betstop-app build (%s %s) --\n\n",
		__DATE__, __TIME__);

	/* Init external libraries */
	xmlInitParser();
	LIBXML_TEST_VERSION
	curl_global_init(CURL_GLOBAL_ALL);

	/* Initialize betstop */
	ret = betstop_init(&bs);
	if (ret != BS_ENONE)
		return EXIT_FAILURE;

	bs_dbg("Loading all betstop data...\n");
	betstop_load_data(&bs);

	/* Add bookies to betstop */
	for (i = 0; i < ARRAY_SIZE(bookies); i++) {
		bk = bookies[i].bk;
		if (bk) {
			len = ARRAY_SIZE(bk->slug);
			snprintf(bk->slug, len, "%s", bookies[i].name);

			betstop_bookie_add(&bs, bk);
		}
	}

	betstop_match_load(&bs, "handball");
	betstop_match_load(&bs, "soccer");

	betstop_arb_gen(&bs);

	betstop_finalize(&bs);

	/* Finalize external libraries */
	curl_global_cleanup();
	xmlMemoryDump();
	xmlCleanupParser();

	printf("\n-- Done --\n");

	return (EXIT_SUCCESS);
}

/* End of file */
