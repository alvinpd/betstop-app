/*
 * File:   bookie.h
 * Author: Alvin Difuntorum <alvinpd09@gmail.com>
 *
 * Created on October 30, 2012, 8:31 PM
 */

#ifndef __INCLUDE_BOOKIE_H__
#define	__INCLUDE_BOOKIE_H__

#include <time.h>

#include "common.h"
#include "list.h"

struct betstop;

#define BS_BET_MONEYLINE		1
#define BS_BET_3WAY			2
#define BS_BET_ASIAN_HANDICAP		3
#define BS_BET_DOUBLE_CHANCE		4
#define BS_BET_EUROPEAN_HANDICAP	5
#define BS_BET_OVER_UNDER		6

/*
 * Structure to define a match provided by a particular bookie.
 *
 * @type    Use to check what type of bet is being added to the database. Uses
 *          any of the the BS_BET_* defines
 * @ml*     Odds for the Moneyline bet type
 * @odd*    Odds for the 3-Way bet type
 * @ah*     Odds for the Asian Handicap bet type
 * @dc*     Odds for the Double Chance bet type
 * @ou*     Odds for the Over/Under bet type
 * @sched   The schedule of the match in unix time format and UTC+0 timezone
 * @bookie  The bookie id of this match
 * @league  The league id of this match
 * @team1   The id of the first team of this match
 * @team2   The id of the second team of this match
 */
struct match {
	int    type;

	double ml1, ml2;
	double odd1, odd2, odd3;
	double ah1, ah2;
	double dc1, dc2;
	double eh1, eh2;
	double ou1, ou2;

	time_t sched;

	unsigned long bookie;
	unsigned long league;

	unsigned long team1;
	unsigned long team2;
};

struct bookie_data {
	char   sport[32];

	char   fname[BS_MAX_PATH];
	char   url[BS_MAX_URL];
	size_t size;
	time_t dltime;
};

struct bookie {
	struct list_head list;

	struct betstop *bs;

	char sport[32];
	pthread_t tid;

	char slug[32];

	struct match     match;
	struct list_head teams;

	int (*fetch)(struct bookie *bk, const char *url, const char *fname);
	int (*parse)(struct bookie *bk, const char *fname);
};

struct team {
	struct list_head list;

	char name[64];
	unsigned long id;
};

unsigned long bookie_team_get_id(struct bookie *bk, const char *name);

#endif	/* __INCLUDE_BOOKIE_H__ */
