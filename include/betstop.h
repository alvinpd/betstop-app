/*
 * File:   betstop.h
 * Author: Alvin Difuntorum <alvinpd09@gmail.com>
 *
 * Created on October 27, 2012, 8:49 AM
 */

#ifndef __INCLUDE_BETSTOP_H__
#define	__INCLUDE_BETSTOP_H__

#include <pthread.h>
#include <sqlite3.h>

#include <curl/curl.h>
#include <libxml/tree.h>
#include <libxml/parser.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>
#include <libxml/HTMLparser.h>
#include <libxml/HTMLtree.h>

#include "common.h"
#include "list.h"

/*
 * BetStop defaults
 */

#define BS_MAX_PATH		255
#define BS_MAX_URL		512
#define BS_DATA_CACHE		"cache"
#define BS_DEF_USERAGENT	"Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)"

/*
 * BetStop error definitions
 */

#define BS_ENONE		0
#define BS_EPERM		1
#define BS_ENOENT		2
#define BS_ENOMEM		12
#define BS_EBUSY		16
#define BS_EEXIST		17
#define BS_EINVAL		22

#define bs_dbg(msg,...)		fprintf(stdout, "betstop: " msg, ##__VA_ARGS__)
#define bs_err(msg,...)		fprintf(stderr, "betstop: " msg, ##__VA_ARGS__)
#define bs_info(msg,...)	fprintf(stdout, "betstop: " msg, ##__VA_ARGS__)

#include "bookie.h"
#include "database.h"

/*
 * Data structures
 */

struct arbitrage;

struct receive {
	int    fd;
	size_t size;
};

struct betstop {
	char	dbpath[BS_MAX_PATH];
	sqlite3	*db;
	pthread_mutex_t dbmtx;

	FILE    *ferr;

	struct list_head bookies;
	struct list_head competitions;
};

struct competition {
	struct list_head list;

	int    count; /* Total number of odds */
	u64    team1;
	u64    team2;
	u64    league;
	time_t sched;

	int    arb_total;
	struct arbitrage *arbs;
	struct list_head odds;
};

struct odd {
	struct list_head list;

	u64 bookie;

	double odd1;
	double odd2;
	double odd3;
};

struct arbitrage {
	struct odd *arb1;
	struct odd *arb2;
	struct odd *arb3;
};

/*
 * BetStop API functions
 */

int  betstop_init(struct betstop *bs);
void betstop_finalize(struct betstop *bs);

xmlXPathObjectPtr betstop_nodeset(xmlDocPtr doc, xmlChar *xpath);

size_t betstop_write(void *buffer, size_t size, size_t nmemb, void *stream);

int betstop_load_data(struct betstop *bs);

int betstop_bookie_add(struct betstop *bs, struct bookie *bk);

int betstop_match_add(struct betstop *bs, struct match *m);
int betstop_match_load(struct betstop *bs, const char *sport);

int betstop_arb_gen(struct betstop *bs);

/*
 * Utility functions
 */

void betstop_str_trim(const char *in, char *out);

#endif	/* __INCLUDE_BETSTOP_H__ */
