/*
 * File:   database.h
 * Author: Alvin Difuntorum <alvinpd09@gmail.com>
 *
 * Created on November 1, 2012, 4:46 PM
 */

#ifndef __INCLUDE_DATABASE_H__
#define __INCLUDE_DATABASE_H__

struct betstop;

int betstop_db_query_id(struct betstop *bs, const char *query, unsigned long *id);

#endif	/* __INCLUDE_DATABASE_H__ */

