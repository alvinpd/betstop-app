/*
 * File:   interwetten.c
 * Author: Alvin Difuntorum <alvinpd09@gmail.com>
 *
 *
 */

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "betstop.h"

#define INTERWETTEN_BOOKIE	""

struct interwetten_parser {
	struct tm     tm;
	struct match  *m;
	struct bookie *bk;
};

static void
iw_parse_odds(struct interwetten_parser *ip, xmlDocPtr doc, xmlNodePtr cur)
{
	xmlChar *c, *v, *p;
	struct match  *m = ip->m;
	struct bookie *bk = ip->bk;

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		c = NULL;
		if (!xmlStrcmp(cur->name, (const xmlChar *) "span"))
			c = xmlGetProp(cur, (const xmlChar *) "class");

		if (c && !xmlStrcmp(c, (const xmlChar *) "name")) {
			v = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
			if (v) {
				if (xmlStrcmp(v, (const xmlChar *) "X")) {
					if (m->team1 == 0)
						m->team1 =
							bookie_team_get_id(bk, v);
					else if (m->team2 == 0)
						m->team2 =
							bookie_team_get_id(bk, v);
				}
				xmlFree(v);
			}
		} else if (c && !xmlStrcmp(c, (const xmlChar *) "odds")) {
			v = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
			if (v) {
				p = v;
				while (p++) {
					if (*p == ',') {
						*p = '.';
						break;
					}
				}

				if (m->odd1 == 0)
					m->odd1 = strtod(v, NULL);
				else if (m->odd2 == 0)
					m->odd2 = strtod(v, NULL);
				else if (m->odd3 == 0)
					m->odd3 = strtod(v, NULL);

				xmlFree(v);
			}
		}

		if (c)
			xmlFree(NULL);
		cur = cur->next;
	}
}

static void
iw_parse_team(struct interwetten_parser *ip, xmlDocPtr doc, xmlNodePtr cur)
{
	xmlChar *c;
	struct match *m = ip->m;
	struct betstop *bs = ip->bk->bs;

	m->odd1 = 0;
	m->odd2 = 0;
	m->odd3 = 0;
	m->team1 = 0;
	m->team2 = 0;

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "table"))
			break;
		cur = cur->next;
	}

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "tr"))
			break;
		cur = cur->next;
	}

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		c = NULL;
		if (!xmlStrcmp(cur->name, (const xmlChar *) "td"))
			c = xmlGetProp(cur, (const xmlChar *) "class");

		if (c && xmlStrstr(c, (const xmlChar *) "offer3"))
			iw_parse_odds(ip, doc, cur);

		if (c)
			xmlFree(c);

		cur = cur->next;
	}

	betstop_match_add(bs, m);
}

static void
iw_parse_match(struct interwetten_parser *ip, xmlDocPtr doc, xmlNodePtr cur)
{
	char time[10] = { 0, };
	xmlChar *c, *v;
	struct match *m = ip->m;

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		c = NULL;
		if (!xmlStrcmp(cur->name, (const xmlChar *) "div"))
			c = xmlGetProp(cur, (const xmlChar *) "class");

		if (c && xmlStrstr(c, (const xmlChar *) "time2")) {
			v = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
			if (v) {
				betstop_str_trim(v, time);
				strptime(time, "%H:%M", &ip->tm);

				m->sched = mktime(&ip->tm) - (9 * 3600);
				xmlFree(v);
			}
		} else if (c && xmlStrstr(c, (const xmlChar *) "group_center")) {
			iw_parse_team(ip, doc, cur);
		}

		if (c)
			xmlFree(c);
		cur = cur->next;
	}
}

static void
iw_parse_table(struct interwetten_parser *ip, xmlDocPtr doc, xmlNodePtr cur)
{
	xmlChar *c, *v;

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "div"))
			break;
		cur = cur->next;
	}

	if (!cur)
		return;

	while (cur != NULL) {
		c = NULL;
		if (!xmlStrcmp(cur->name, (const xmlChar *) "div"))
			c = xmlGetProp(cur, (const xmlChar *) "class");

		if (c && xmlStrstr(c, (const xmlChar *) "offerDate")) {
			v = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
			if (v) {
				memset(&ip->tm, 0, sizeof(ip->tm));
				strptime(v, "%d.%m.%y", &ip->tm);
				ip->tm.tm_isdst = -1;
				xmlFree(v);
			}
		} else if (c && xmlStrstr(c, (const xmlChar *) "offergroup normal")) {
			iw_parse_match(ip, doc, cur);
		}

		if (c)
			xmlFree(c);

		cur = cur->next;
	}
}

static int interwetten_parse(struct bookie *bk, const char *fname)
{
	int    fd, ret;
	char   *htmlbuff;
	struct stat st;

	htmlDocPtr        doc;
	xmlNodeSetPtr     nodeset;
	xmlXPathObjectPtr nodes;
	htmlParserCtxtPtr parser;

	bs_info("Parsing bookie file: %s\n", fname);

	ret = stat(fname, &st);
	if (ret != 0) {
		bs_err("File not found: %s\n", fname);
		return BS_EINVAL;
	}

	htmlbuff = malloc(st.st_size);
	if (!htmlbuff) {
		bs_err("Unable to allocate %lu bytes\n", (u64) st.st_size);
		return -BS_ENOMEM;
	}

	fd = open(fname, O_RDONLY);
	if (fd < 0) {
		ret = -BS_ENOENT;
		goto fail_open;
	}

	parser = htmlCreateMemoryParserCtxt(htmlbuff, st.st_size);
	if (!parser) {
		bs_err("Failed to create an html parser context\n");
		ret = -BS_ENOMEM;
		goto fail_ctxt;
	}

	htmlCtxtUseOptions(parser, HTML_PARSE_NOBLANKS | HTML_PARSE_NOERROR |
				   HTML_PARSE_NOWARNING | HTML_PARSE_NONET);

	doc = htmlCtxtReadFd(parser, fd, NULL, NULL, 0);
	if (!doc) {
		bs_err("Failed to open the html document\n");
		ret = -BS_EINVAL;
		goto fail_readfd;
	}

	nodes = betstop_nodeset(doc, (xmlChar*) "//div[@class='containerContentTable']/div");
	if (nodes) {
		int    i;
		struct interwetten_parser ip;

		ip.m = &bk->match;
		ip.bk = bk;

		nodeset = nodes->nodesetval;
		for (i = 0; i < nodeset->nodeNr; i++)
			iw_parse_table(&ip, doc, nodeset->nodeTab[i]);

		xmlXPathFreeObject(nodes);
	}

	xmlFreeDoc(doc);

fail_readfd:
	htmlFreeParserCtxt(parser);

fail_ctxt:
	close(fd);

fail_open:
	free(htmlbuff);

	return 0;

}

static int interwetten_fetch(struct bookie *bk, const char *url, const char *fname)
{
	int         ret = BS_ENONE;
	struct curl *curl;
	struct receive rcv;

	curl = curl_easy_init();
	if (!curl)
		return -BS_ENOMEM;

	rcv.size = 0;
	rcv.fd = open(fname, O_WRONLY | O_CREAT,
			S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
	if (rcv.fd < 0) {
		ret = -BS_ENOENT;
		goto exit_curl;
	}

	curl_easy_setopt(curl, CURLOPT_URL, url);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, betstop_write);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *) &rcv);
	curl_easy_setopt(curl, CURLOPT_COOKIE, INTERWETTEN_BOOKIE);
	curl_easy_setopt(curl, CURLOPT_USERAGENT, BS_DEF_USERAGENT);

	bs_info("Fetching sport data: url(%s) fname(%s)...\n", url, fname);

	ret = curl_easy_perform(curl);
	if(ret != CURLE_OK) {
		bs_err("curl_easy_perform failed (%s)\n",
			    curl_easy_strerror(ret));
		ret = -BS_ENOENT;
	} else {
		bs_info("Fetched %ld bytes\n", rcv.size);
		ret = rcv.size > 0 ? BS_ENONE : -BS_ENOENT;
	}

	close(rcv.fd);

exit_curl:
	curl_easy_cleanup(curl);

	return ret;
}

struct bookie interwetten = {
	.fetch = interwetten_fetch,
	.parse = interwetten_parse,
};

/* End of file */
