/*
 * File:   gamebookers.c
 * Author: Alvin Difuntorum <alvinpd09@gmail.com>
 *
 *
 */

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "betstop.h"

#define GB_COOKIE	""

struct gb_parser {
	struct tm     tm;
	struct match  *m;
	struct bookie *bk;
};

static void gb_parse_team(struct gb_parser *gp, xmlDocPtr doc, xmlNodePtr cur)
{
	int draw = 0;
	xmlChar *c, *v;
	struct match *m = gp->m;
	struct bookie *bk = gp->bk;

	c = xmlGetProp(cur, (const xmlChar *) "class");
	if (c) {
		if (xmlStrstr(c, (const xmlChar *) "three-way"))
			draw = 1;
		xmlFree(c);
	}

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "form"))
			break;
		cur = cur->next;
	}

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "button"))
			break;
		cur = cur->next;
	}

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		c = NULL;
		if (!xmlStrcmp(cur->name, (const xmlChar *) "span"))
			c = xmlGetProp(cur, (const xmlChar *) "class");

		if (c && !xmlStrcmp(c, (const xmlChar *) "odds")) {
			v = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
			if (v) {
				if (m->odd1 == 0)
					m->odd1 = strtod(v, NULL);
				else if (m->odd2 == 0)
					m->odd2 = strtod(v, NULL);
				else if (m->odd3 == 0)
					m->odd3 = strtod(v, NULL);

				xmlFree(v);
			}
		} else if (c && !xmlStrcmp(c, (const xmlChar *) "option-name")) {
			v = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
			if (v) {
				if (!draw && m->team1 == 0)
					m->team1 = bookie_team_get_id(bk, v);
				else if (!draw && m->team2 == 0)
					m->team2 = bookie_team_get_id(bk, v);

				xmlFree(v);
			}
		}

		if (c)
			xmlFree(c);
		cur = cur->next;
	}
}

static void gb_parse_option(struct gb_parser *gp, xmlDocPtr doc, xmlNodePtr cur)
{
	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "tr"))
			break;
		cur = cur->next;
	}

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "td"))
			gb_parse_team(gp, doc, cur);

		cur = cur->next;
	}
}

static void gb_parse_listing(struct gb_parser *gp, xmlDocPtr doc, xmlNodePtr cur)
{
	char time[10];
	xmlChar *v;
	struct match *m = gp->m;
	struct betstop *bs = gp->bk->bs;

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "div"))
			break;
		cur = cur->next;
	}

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "div"))
			break;
		cur = cur->next;
	}

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "ul"))
			break;
		cur = cur->next;
	}

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "li"))
			break;
		cur = cur->next;
	}

	m->odd1 = 0;
	m->odd2 = 0;
	m->odd3 = 0;
	m->team1 = 0;
	m->team2 = 0;

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "h6")) {
			v = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
			if (v) {
				memset(time, 0, ARRAY_SIZE(time));
				betstop_str_trim(v, time);
				strptime(time, "%H:%M %p", &gp->tm);
				xmlFree(v);
			}
		} else if (!xmlStrcmp(cur->name, (const xmlChar *) "table")) {
			gb_parse_option(gp, doc, cur);
		}

		cur = cur->next;
	}

	m->sched = mktime(&gp->tm) - (8 * 3600);
	betstop_match_add(bs, m);
}

static void gb_parse_group(struct gb_parser *gp, xmlDocPtr doc, xmlNodePtr cur)
{
	char *c, date[64];
	xmlChar *v;

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "h2"))
			break;
		cur = cur->next;
	}

	v = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
	if (v) {
		memset(&gp->tm, 0, sizeof(gp->tm));
		memset(date, 0, ARRAY_SIZE(date));
		betstop_str_trim(v, date);
		c = strstr(date, " - ");
		if (c != NULL) {
			c += 3;
			strptime(c, "%m/%d/%Y", &gp->tm);
		}
		xmlFree(v);
	}

	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "ul"))
			break;
		cur = cur->next;
	}

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "li"))
			gb_parse_listing(gp, doc, cur);
		cur = cur->next;
	}
}

static void gb_parse_entry(struct gb_parser *gp, xmlDocPtr doc, xmlNodePtr cur)
{
	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "div"))
			break;
		cur = cur->next;
	}

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "ul"))
			break;
		cur = cur->next;
	}

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "li"))
			break;
		cur = cur->next;
	}

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "ul"))
			break;
		cur = cur->next;
	}

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "li"))
			gb_parse_group(gp, doc, cur);
		cur = cur->next;
	}
}

static int gamebookers_parse(struct bookie *bk, const char *fname)
{
	int          fd, ret;
	char         *htmlbuff;
	struct stat  st;

	htmlDocPtr        doc;
	xmlNodeSetPtr     nodeset;
	xmlXPathObjectPtr nodes;
	htmlParserCtxtPtr parser;

	bs_info("Parsing bookie file: %s\n", fname);

	ret = stat(fname, &st);
	if (ret != 0) {
		bs_err("File not found: %s\n", fname);
		return BS_EINVAL;
	}

	htmlbuff = malloc(st.st_size);
	if (!htmlbuff) {
		bs_err("Unable to allocate %lu bytes\n", (u64) st.st_size);
		return -BS_ENOMEM;
	}

	fd = open(fname, O_RDONLY);
	if (fd < 0) {
		ret = -BS_ENOENT;
		goto fail_open;
	}

	parser = htmlCreateMemoryParserCtxt(htmlbuff, st.st_size);
	if (!parser) {
		bs_err("Failed to create an html parser context\n");
		ret = -BS_ENOMEM;
		goto fail_ctxt;
	}

	htmlCtxtUseOptions(parser, HTML_PARSE_NOBLANKS | HTML_PARSE_NOERROR |
				   HTML_PARSE_NOWARNING | HTML_PARSE_NONET);

	doc = htmlCtxtReadFd(parser, fd, NULL, NULL, 0);
	if (!doc) {
		bs_err("Failed to open the html document\n");
		ret = -BS_EINVAL;
		goto fail_readfd;
	}

	nodes = betstop_nodeset(doc, (xmlChar*)
			"//div[@id='markets']/div[@class='ui-widget-content']");
	if (nodes) {
		int    i;
		struct gb_parser gp;

		gp.m = &bk->match;
		gp.bk = bk;

		nodeset = nodes->nodesetval;
		for (i = 0; i < nodeset->nodeNr; i++)
			gb_parse_entry(&gp, doc, nodeset->nodeTab[i]);

		xmlXPathFreeObject(nodes);
	}

	xmlFreeDoc(doc);

	fail_readfd:
	htmlFreeParserCtxt(parser);

fail_ctxt:
	close(fd);

fail_open:
	free(htmlbuff);

	return ret;
}

static int gamebookers_fetch(struct bookie *bk, const char *url, const char *fname)
{
	int         ret = BS_ENONE;
	struct curl *curl;
	struct receive rcv;

	curl = curl_easy_init();
	if (!curl)
		return -BS_ENOMEM;

	rcv.size = 0;
	rcv.fd = open(fname, O_WRONLY | O_CREAT,
			S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
	if (rcv.fd < 0) {
		ret = -BS_ENOENT;
		goto exit_curl;
	}

	curl_easy_setopt(curl, CURLOPT_URL, url);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, betstop_write);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *) &rcv);
	curl_easy_setopt(curl, CURLOPT_COOKIE, GB_COOKIE);
	curl_easy_setopt(curl, CURLOPT_USERAGENT, BS_DEF_USERAGENT);

	bs_info("Fetching sport data: url(%s) fname(%s)...\n", url, fname);

	ret = curl_easy_perform(curl);
	if(ret != CURLE_OK) {
		bs_err("curl_easy_perform failed (%s)\n",
			    curl_easy_strerror(ret));
		ret = -BS_ENOENT;
	} else {
		bs_info("Fetched %ld bytes\n", rcv.size);
		ret = rcv.size > 0 ? BS_ENONE : -BS_ENOENT;
	}

	close(rcv.fd);

exit_curl:
	curl_easy_cleanup(curl);

	return ret;
}

struct bookie gamebookers = {
	.fetch = gamebookers_fetch,
	.parse = gamebookers_parse,
};

/* End of file */
