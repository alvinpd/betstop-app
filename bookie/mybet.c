/*
 * File:   mybet.c
 * Author: Alvin Difuntorum <alvinpd09@gmail.com>
 *
 *
 */

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "betstop.h"

#define MYBET_COOKIE		""

struct mybet_parser {
	struct match  *m;
	struct bookie *bk;
};

static void
mb_parse_event(struct mybet_parser *mp, xmlDocPtr doc, xmlNodePtr cur)
{
	char team[32] = {0, };
	xmlChar *c, *e, *v, *s, *t;
	xmlNodePtr tmp;
	struct tm tm;
	struct match  *m = mp->m;
	struct bookie *bk = mp->bk;

	cur = cur->xmlChildrenNode;
	while (cur) {
		c = NULL;
		if (!xmlStrcmp(cur->name, (const xmlChar *) "p"))
			c = xmlGetProp(cur, (const xmlChar *) "class");

		if (c && !xmlStrcmp(c, (const xmlChar *) "eventName")) {
			tmp = cur->xmlChildrenNode;
			while (tmp != NULL) {
				if (!xmlStrcmp(tmp->name, (const xmlChar *) "span")) {
					v = xmlNodeListGetString(doc,
						tmp->xmlChildrenNode, 1);
					if (v) {
						e = strstr(v, ":");
						memcpy(team, v, e - v);
						m->team1 = bookie_team_get_id(
							bk, team);

						e++;
						m->team2 = bookie_team_get_id(
							bk, e);
						xmlFree(v);
					}
				}
				tmp = tmp->next;
			}
		} else if (c && !xmlStrcmp(c, (const xmlChar *) "eventData")) {
			tmp = cur->xmlChildrenNode;
			while (tmp != NULL) {
				s = NULL;
				t = NULL;

				if (!xmlStrcmp(tmp->name, (const xmlChar *) "span")) {
					s = xmlGetProp(tmp,
						(const xmlChar *) "class");
					t = xmlGetProp(tmp,
						(const xmlChar *) "content");
				}

				if (s && !xmlStrcmp(s, (const xmlChar *) "startTime")) {
					if (t) {
						memset(&tm, 0, sizeof(tm));
						strptime(t, "%Y-%m-%dT%H:%M", &tm);
					}
				}

				if (s)
					xmlFree(s);

				if (t)
					xmlFree(t);

				tmp = tmp->next;
			}
		}

		if (c)
			xmlFree(c);
		cur = cur->next;
	}

	m->sched = mktime(&tm);
}

static void
mb_parse_odd(struct mybet_parser *mp, xmlDocPtr doc, xmlNodePtr cur, double *o)
{
	xmlChar *v;

	cur = cur->xmlChildrenNode;
	while (cur) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "p"))
			break;
		cur = cur->next;
	}

	cur = cur->xmlChildrenNode;
	while (cur) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "a")) {
			v = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
			if (v) {
				*o = strtod(v, NULL);
				xmlFree(v);
			}
		}
		cur = cur->next;
	}
}

static void
mb_parse_table(struct mybet_parser *mp, xmlDocPtr doc, xmlNodePtr cur)
{
	xmlChar *c;
	struct match *m = mp->m;
	struct betstop *bs = mp->bk->bs;

	m->odd1 = 0;
	m->odd2 = 0;
	m->odd3 = 0;
	m->team1 = 0;
	m->team2 = 0;

	cur = cur->xmlChildrenNode;
	while (cur) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "tr"))
			break;
		cur = cur->next;
	}

	cur = cur->xmlChildrenNode;
	while (cur) {
		c = NULL;
		if (!xmlStrcmp(cur->name, (const xmlChar *) "td"))
			c = xmlGetProp(cur, (const xmlChar *) "class");

		if (c && !xmlStrcmp(c, (const xmlChar *) "eventInfo"))
			mb_parse_event(mp, doc, cur);
		else if (c && !xmlStrcmp(c, (const xmlChar *) "home1x2"))
			mb_parse_odd(mp, doc, cur, &m->odd1);
		else if (c && !xmlStrcmp(c, (const xmlChar *) "draw1x2"))
			mb_parse_odd(mp, doc, cur, &m->odd2);
		else if (c && !xmlStrcmp(c, (const xmlChar *) "away1x2"))
			mb_parse_odd(mp, doc, cur, &m->odd3);

		if (c)
			xmlFree(c);
		cur = cur->next;
	}

	betstop_match_add(bs, m);
}

static int mybet_parse(struct bookie *bk, const char *fname)
{
	int    fd, ret;
	char   *htmlbuff;
	struct stat st;

	htmlDocPtr        doc;
	xmlNodeSetPtr     nodeset;
	xmlXPathObjectPtr nodes;
	htmlParserCtxtPtr parser;

	bs_info("Parsing bookie file: %s\n", fname);

	ret = stat(fname, &st);
	if (ret != 0) {
		bs_err("File not found: %s\n", fname);
		return BS_EINVAL;
	}

	htmlbuff = malloc(st.st_size);
	if (!htmlbuff) {
		bs_err("Unable to allocate %lu bytes\n", (u64) st.st_size);
		return -BS_ENOMEM;
	}

	fd = open(fname, O_RDONLY);
	if (fd < 0) {
		ret = -BS_ENOENT;
		goto fail_open;
	}

	parser = htmlCreateMemoryParserCtxt(htmlbuff, st.st_size);
	if (!parser) {
		bs_err("Failed to create an html parser context\n");
		ret = -BS_ENOMEM;
		goto fail_ctxt;
	}

	htmlCtxtUseOptions(parser, HTML_PARSE_NOBLANKS | HTML_PARSE_NOERROR |
				   HTML_PARSE_NOWARNING | HTML_PARSE_NONET);

	doc = htmlCtxtReadFd(parser, fd, NULL, NULL, 0);
	if (!doc) {
		bs_err("Failed to open the html document\n");
		ret = -BS_EINVAL;
		goto fail_readfd;
	}

	nodes = betstop_nodeset(doc, (xmlChar*) "//table[@typeof='v:Event']");
	if (nodes) {
		int    i;
		struct mybet_parser mp;

		mp.m = &bk->match;
		mp.bk = bk;

		nodeset = nodes->nodesetval;
		for (i = 0; i < nodeset->nodeNr; i++)
			mb_parse_table(&mp, doc, nodeset->nodeTab[i]);

		xmlXPathFreeObject(nodes);
	}

	xmlFreeDoc(doc);

fail_readfd:
	htmlFreeParserCtxt(parser);

fail_ctxt:
	close(fd);

fail_open:
	free(htmlbuff);

	return 0;
}

static int mybet_fetch(struct bookie *bk, const char *url, const char *fname)
{
	int         ret = BS_ENONE;
	struct curl *curl;
	struct receive rcv;

	curl = curl_easy_init();
	if (!curl)
		return -BS_ENOMEM;

	rcv.size = 0;
	rcv.fd = open(fname, O_WRONLY | O_CREAT,
			S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
	if (rcv.fd < 0) {
		ret = -BS_ENOENT;
		goto exit_curl;
	}

	curl_easy_setopt(curl, CURLOPT_URL, url);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, betstop_write);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *) &rcv);
	curl_easy_setopt(curl, CURLOPT_COOKIE, MYBET_COOKIE);
	curl_easy_setopt(curl, CURLOPT_USERAGENT, BS_DEF_USERAGENT);

	bs_info("Fetching sport data: url(%s) fname(%s)...\n", url, fname);

	ret = curl_easy_perform(curl);
	if(ret != CURLE_OK) {
		bs_err("curl_easy_perform failed (%s)\n",
			    curl_easy_strerror(ret));
		ret = -BS_ENOENT;
	} else {
		bs_info("Fetched %ld bytes\n", rcv.size);
		ret = rcv.size > 0 ? BS_ENONE : -BS_ENOENT;
	}

	close(rcv.fd);

exit_curl:
	curl_easy_cleanup(curl);

	return ret;
}

struct bookie mybet = {
	.fetch = mybet_fetch,
	.parse = mybet_parse,
};

/* End of file */
