/*
 * File:   betfair.c
 * Author: Alvin Difuntorum <alvinpd09@gmail.com>
 *
 *
 */

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "betstop.h"

#define BETFAIR_COOKIE	""

struct betfair_parser {
	struct tm     tm;
	struct match  *m;
	struct bookie *bk;
};

static void bf_parse_date(struct betfair_parser *bp, xmlDocPtr doc, xmlNodePtr cur)
{
	char    date[64];
	xmlChar *v;

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "tr"))
			break;
		cur = cur->next;
	}

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "td"))
			break;
		cur = cur->next;
	}

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "h3"))
			break;
		cur = cur->next;
	}

	v = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
	if (v) {
		memset(&bp->tm, 0, sizeof(bp->tm));
		memset(date, 0, 64);
		betstop_str_trim(v, date);
		strptime(date, "%a, %d %b %Y", &bp->tm);

		xmlFree(v);
	}
}

static void bf_parse_teams(struct betfair_parser *bp, xmlDocPtr doc, xmlNodePtr cur)
{
	char    team[32];
	xmlChar *c, *v;
	xmlNodePtr tmp;
	struct match *m = bp->m;
	struct bookie *bk = bp->bk;

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "tr"))
			break;
		cur = cur->next;
	}

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "td"))
			break;
		cur = cur->next;
	}

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "a")) {
			tmp = cur->xmlChildrenNode;
			while (tmp != NULL) {
				c = NULL;
				if (!xmlStrcmp(tmp->name,
					       (const xmlChar *) "span"))
					c = xmlGetProp(tmp,
						(const xmlChar *) "class");

				memset(team, 0, 32);
				if (c && xmlStrstr(c, (const xmlChar *) "home")) {
					v = xmlNodeListGetString(doc,
						tmp->xmlChildrenNode, 1);
					if (v) {
						betstop_str_trim(v, team);
						m->team1 = bookie_team_get_id(bk,
							team);
						xmlFree(v);
					}
				} else if (c && xmlStrstr(c, (const xmlChar *)
							  "away")) {
					v = xmlNodeListGetString(doc,
						tmp->xmlChildrenNode, 1);
					if (v) {
						betstop_str_trim(v, team);
						m->team2 = bookie_team_get_id(bk,
							team);
						xmlFree(v);
					}
				}

				if (c)
					xmlFree(c);
				tmp = tmp->next;
			}
		} else if (!xmlStrcmp(cur->name, (const xmlChar *) "span")) {
			tmp = cur->xmlChildrenNode;
			while (tmp != NULL) {
				v = NULL;
				if (!xmlStrcmp(tmp->name, (const xmlChar *) "span"))
					v = xmlNodeListGetString(doc,
						tmp->xmlChildrenNode, 1);

				if (v) {
					strptime(v, "%H:%M", &bp->tm);
					xmlFree(v);
				}
				tmp = tmp->next;
			}
		}

		cur = cur->next;
	}
}

static void bf_parse_odds(struct betfair_parser *bp, xmlDocPtr doc, xmlNodePtr cur)
{
	char odd[10];
	xmlChar *c, *v;
	xmlNodePtr tmp;
	struct match *m = bp->m;

	m->odd1 = 0;
	m->odd2 = 0;
	m->odd3 = 0;

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "tr"))
			break;
		cur = cur->next;
	}

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		c = NULL;
		if (!xmlStrcmp(cur->name, (const xmlChar *) "td"))
			c = xmlGetProp(cur, (const xmlChar *) "class");

		if (c && xmlStrstr(c, "odds back")) {
			tmp = cur->xmlChildrenNode;
			while (tmp != NULL) {
				if (!xmlStrcmp(tmp->name,
						(const xmlChar *) "button"))
					break;
				tmp = tmp->next;
			}

			tmp = tmp->xmlChildrenNode;
			while (tmp != NULL) {
				if (!xmlStrcmp(tmp->name,
						(const xmlChar *) "span"))
					break;
				tmp = tmp->next;
			}

			v = xmlNodeListGetString(doc, tmp->xmlChildrenNode, 1);
			if (v) {
				memset(odd, 0, ARRAY_SIZE(odd));
				betstop_str_trim(v, odd);
				if (m->odd1 == 0)
					m->odd1 = strtod(odd, NULL);
				else if (m->odd2 == 0)
					m->odd2 = strtod(odd, NULL);
				else if (m->odd3 == 0)
					m->odd3 = strtod(odd, NULL);

				xmlFree(v);
			}
		}

		if (c)
			xmlFree(c);
		cur = cur->next;
	}
}

static void bf_parse_match(struct betfair_parser *bp, xmlDocPtr doc, xmlNodePtr cur)
{
	xmlChar *p;
	struct match *m = bp->m;
	struct betstop *bs = bp->bk->bs;

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "tr"))
			break;
		cur = cur->next;
	}

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "td"))
			break;
		cur = cur->next;
	}

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		p = NULL;
		if (!xmlStrcmp(cur->name, (const xmlChar *) "table"))
			p = xmlGetProp(cur, (const xmlChar *) "class");

		if (p && xmlStrstr(p, (const xmlChar *) "left"))
			bf_parse_teams(bp, doc, cur);
		else if (p && xmlStrstr(p, (const xmlChar *) "right"))
			bf_parse_odds(bp, doc, cur);

		if (p)
			xmlFree(p);
		cur = cur->next;
	}

	m->sched = mktime(&bp->tm);
	betstop_match_add(bs, m);
}

static void bf_parse_entry(struct betfair_parser *bp, xmlDocPtr doc, xmlNodePtr cur)
{
	xmlChar *p = xmlGetProp(cur, (const xmlChar *) "id");

	if (p == NULL)
		return;

	if (xmlStrstr(p, (const xmlChar *) "-coming-up-heading") != NULL)
		bf_parse_date(bp, doc, cur);
	else if (xmlStrstr(p, (const xmlChar *) "-market") != NULL)
		bf_parse_match(bp, doc, cur);

	if (p)
		xmlFree(p);
}

static int betfair_parse(struct bookie *bk, const char *fname)
{
        int          fd, ret;
        char         *htmlbuff;
        struct stat  st;

        htmlDocPtr        doc;
        xmlNodeSetPtr     nodeset;
        xmlXPathObjectPtr nodes;
        htmlParserCtxtPtr parser;

        bs_info("Parsing bookie file: %s\n", fname);

        ret = stat(fname, &st);
        if (ret != 0) {
                bs_err("File not found: %s\n", fname);
                return BS_EINVAL;
        }

        htmlbuff = malloc(st.st_size);
        if (!htmlbuff) {
                bs_err("Unable to allocate %lu bytes\n", (u64) st.st_size);
                return -BS_ENOMEM;
        }

        fd = open(fname, O_RDONLY);
        if (fd < 0) {
                ret = -BS_ENOENT;
                goto fail_open;
        }

        parser = htmlCreateMemoryParserCtxt(htmlbuff, st.st_size);
        if (!parser) {
                bs_err("Failed to create an html parser context\n");
                ret = -BS_ENOMEM;
                goto fail_ctxt;
        }

        htmlCtxtUseOptions(parser, HTML_PARSE_NOBLANKS | HTML_PARSE_NOERROR |
                                   HTML_PARSE_NOWARNING | HTML_PARSE_NONET);

        doc = htmlCtxtReadFd(parser, fd, NULL, NULL, 0);
        if (!doc) {
                bs_err("Failed to open the html document\n");
                ret = -BS_EINVAL;
                goto fail_readfd;
        }

        nodes = betstop_nodeset(doc, (xmlChar*) "//table/tbody");
        if (nodes) {
                int    i;
                struct betfair_parser bp;

                bp.m = &bk->match;
                bp.bk = bk;

                nodeset = nodes->nodesetval;
                for (i = 0; i < nodeset->nodeNr; i++)
                        bf_parse_entry(&bp, doc, nodeset->nodeTab[i]);

                xmlXPathFreeObject(nodes);
        }

        xmlFreeDoc(doc);

fail_readfd:
        htmlFreeParserCtxt(parser);

fail_ctxt:
        close(fd);

fail_open:
        free(htmlbuff);

        return 0;

}

static int betfair_fetch(struct bookie *bk, const char *url, const char *fname)
{
	int         ret = BS_ENONE;
	struct curl *curl;
	struct receive rcv;

	curl = curl_easy_init();
	if (!curl)
		return -BS_ENOMEM;

	rcv.size = 0;
	rcv.fd = open(fname, O_WRONLY | O_CREAT,
			S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
	if (rcv.fd < 0) {
		ret = -BS_ENOENT;
		goto exit_curl;
	}

	curl_easy_setopt(curl, CURLOPT_URL, url);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, betstop_write);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *) &rcv);
	curl_easy_setopt(curl, CURLOPT_COOKIE, BETFAIR_COOKIE);
	curl_easy_setopt(curl, CURLOPT_USERAGENT, BS_DEF_USERAGENT);

	bs_info("Fetching sport data: url(%s) fname(%s)...\n", url, fname);

	ret = curl_easy_perform(curl);
	if(ret != CURLE_OK) {
		bs_err("curl_easy_perform failed (%s)\n",
			    curl_easy_strerror(ret));
		ret = -BS_ENOENT;
	} else {
		bs_info("Fetched %ld bytes\n", rcv.size);
		ret = rcv.size > 0 ? BS_ENONE : -BS_ENOENT;
	}

	close(rcv.fd);

exit_curl:
	curl_easy_cleanup(curl);

	return ret;
}


struct bookie betfair = {
	.fetch = betfair_fetch,
	.parse = betfair_parse,
};

/* End of file */
