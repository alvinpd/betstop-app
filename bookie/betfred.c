/*
 * File:   betfred.c
 * Author: Alvin Difuntorum <alvinpd09@gmail.com>
 *
 *
 */

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "betstop.h"

#define BETFRED_COOKIE		""

struct betfred_parser {
	struct match  *m;
	struct bookie *bk;
};

static void
bf_parse_bet(struct betfred_parser *bp, xmlDocPtr doc, xmlNodePtr cur)
{
	double u, d;
	xmlChar *v;
	struct match *m = bp->m;

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		v = NULL;

		if (!xmlStrcmp(cur->name, (const xmlChar *) "currentpriceup")) {
			v = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
			if (v)
				u = strtod(v, NULL);
		} else if (!xmlStrcmp(cur->name, (const xmlChar *) "currentpricedown")) {
			v = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
			if (v)
				d = strtod(v, NULL);
		}

		if (v)
			xmlFree(v);

		cur = cur->next;
	}

	if (m->odd1 == 0)
		m->odd1 = u / d + 1;
	else if (m->odd2 == 0)
		m->odd2 = u / d + 1;
	else if (m->odd3 == 0)
		m->odd3 = u / d + 1;
}

static void
bf_parse_selection(struct betfred_parser *bp, xmlDocPtr doc, xmlNodePtr cur)
{
	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "selection"))
			bf_parse_bet(bp, doc, cur);

		cur = cur->next;
	}
}

static void
bf_parse_market(struct betfred_parser *bp, xmlDocPtr doc, xmlNodePtr cur)
{
	xmlChar   *v;
	struct tm tm;
	struct match *m = bp->m;
	struct bookie *bk = bp->bk;
	struct betstop *bs = bp->bk->bs;

	m->odd1 = 0;
	m->odd2 = 0;
	m->odd3 = 0;

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		v = NULL;

		if (!xmlStrcmp(cur->name, (const xmlChar *) "participantname_home")) {
			v = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
			if (v)
				m->team1 = bookie_team_get_id(bk, v);
		} else if (!xmlStrcmp(cur->name, (const xmlChar *) "participantname_away")) {
			v = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
			if (v)
				m->team2 = bookie_team_get_id(bk, v);
		} else if (!xmlStrcmp(cur->name, (const xmlChar *) "tsbetend")) {
			v = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
			if (v) {
				memset(&tm, 0, sizeof(tm));
				strptime(v, "%Y-%m-%dT%H:%M", &tm);
				tm.tm_isdst = -1;
			}
		} else if (!xmlStrcmp(cur->name, (const xmlChar *) "selections")) {
			bf_parse_selection(bp, doc, cur);
		}

		if (v)
			xmlFree(v);

		cur = cur->next;
	}

	m->sched = mktime(&tm);
	betstop_match_add(bs, m);
}

static int betfred_parse(struct bookie *bk, const char *fname)
{
        int          fd, ret;
        char         *htmlbuff;
        struct stat  st;

        htmlDocPtr        doc;
        xmlNodeSetPtr     nodeset;
        xmlXPathObjectPtr nodes;
        htmlParserCtxtPtr parser;

        bs_info("Parsing bookie file: %s\n", fname);

        ret = stat(fname, &st);
        if (ret != 0) {
                bs_err("File not found: %s\n", fname);
                return BS_EINVAL;
        }

        htmlbuff = malloc(st.st_size);
        if (!htmlbuff) {
                bs_err("Unable to allocate %lu bytes\n", (u64) st.st_size);
                return -BS_ENOMEM;
        }

        fd = open(fname, O_RDONLY);
        if (fd < 0) {
                ret = -BS_ENOENT;
                goto fail_open;
        }

        parser = htmlCreateMemoryParserCtxt(htmlbuff, st.st_size);
        if (!parser) {
                bs_err("Failed to create an html parser context\n");
                ret = -BS_ENOMEM;
                goto fail_ctxt;
        }

        htmlCtxtUseOptions(parser, HTML_PARSE_NOBLANKS | HTML_PARSE_NOERROR |
                                   HTML_PARSE_NOWARNING | HTML_PARSE_NONET);

        doc = htmlCtxtReadFd(parser, fd, NULL, NULL, 0);
        if (!doc) {
                bs_err("Failed to open the html document\n");
                ret = -BS_EINVAL;
                goto fail_readfd;
        }

        nodes = betstop_nodeset(doc, (xmlChar*) "//marketgroup/markets/market");
        if (nodes) {
                int    i;
                struct betfred_parser bp;

                bp.m = &bk->match;
                bp.bk = bk;

                nodeset = nodes->nodesetval;
                for (i = 0; i < nodeset->nodeNr; i++)
                        bf_parse_market(&bp, doc, nodeset->nodeTab[i]);

                xmlXPathFreeObject(nodes);
        }

        xmlFreeDoc(doc);

fail_readfd:
        htmlFreeParserCtxt(parser);

fail_ctxt:
        close(fd);

fail_open:
        free(htmlbuff);

        return 0;

}

static int betfred_fetch(struct bookie *bk, const char *url, const char *fname)
{
	int         ret = BS_ENONE;
	struct curl *curl;
	struct receive rcv;

	curl = curl_easy_init();
	if (!curl)
		return -BS_ENOMEM;

	rcv.size = 0;
	rcv.fd = open(fname, O_WRONLY | O_CREAT,
			S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
	if (rcv.fd < 0) {
		ret = -BS_ENOENT;
		goto exit_curl;
	}

	curl_easy_setopt(curl, CURLOPT_URL, url);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, betstop_write);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *) &rcv);
	curl_easy_setopt(curl, CURLOPT_COOKIE, BETFRED_COOKIE);
	curl_easy_setopt(curl, CURLOPT_USERAGENT, BS_DEF_USERAGENT);

	bs_info("Fetching sport data: url(%s) fname(%s)...\n", url, fname);

	ret = curl_easy_perform(curl);
	if(ret != CURLE_OK) {
		bs_err("curl_easy_perform failed (%s)\n",
			    curl_easy_strerror(ret));
		ret = -BS_ENOENT;
	} else {
		bs_info("Fetched %ld bytes\n", rcv.size);
		ret = rcv.size > 0 ? BS_ENONE : -BS_ENOENT;
	}

	close(rcv.fd);

exit_curl:
	curl_easy_cleanup(curl);

	return ret;
}

struct bookie betfred = {
	.fetch = betfred_fetch,
	.parse = betfred_parse,
};

/* End of file */
