/*
 * File:   betboo.c
 * Author: Alvin Difuntorum <alvinpd09@gmail.com>
 *
 *
 */

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "betstop.h"

#define BETBOO_COOKIE	""

struct bboo_parser {
	struct tm     tm;
	struct match  *m;
	struct bookie *bk;
};

static void bb_parse_str(const char *in, char *out)
{
	char *start, *end;

	start = (char *)in;

	while (*start++ < 0x21);
	start--;

	end = (char*)(in + strlen(in));
	while (*end-- < 0x21);

	memcpy(out, start, end - start + 2);
}

static void bb_parse_date(struct bboo_parser *bp, xmlDocPtr doc, xmlNodePtr cur)
{
	char    sched[64] = {0, };
	time_t  tmt;
	xmlChar *v;
	struct tm *tm = &bp->tm;

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "tr"))
			break;

		cur = cur->next;
	}

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "th"))
			break;

		cur = cur->next;
	}

	tmt = time(NULL);
	memcpy(tm, gmtime(&tmt), sizeof(*tm));
	tm->tm_sec = 0;
	tm->tm_isdst = -1;

	v = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
	if (v) {
		bb_parse_str((const char *)v, sched);
		strptime(sched, "%a, %d %b", tm);
		xmlFree(v);
	}
}

static void bb_parse_team(struct bboo_parser *bp, xmlDocPtr doc, xmlNodePtr cur)
{
	char    team[64];
	double  odd;
	xmlChar *p;
	struct match *m = bp->m;
	struct bookie *bk = bp->bk;

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "a"))
			break;

		cur = cur->next;
	}

	p = xmlGetProp(cur, (const xmlChar *) "title");
	if (p) {
		char *end;

		snprintf(team, 64, "%s", p);
		end = (char*)(&team[0] + strlen(team));

		do {
			if (*end > 0x32)
				break;
			*end = 0;
			end --;
		} while (end > &team[0]);

		xmlFree(p);
	}

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "span"))
			break;

		cur = cur->next;
	}

	p = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
	if (p != NULL) {
		odd = strtod(p, NULL);
		xmlFree(p);
	}

	if (m->odd1 == 0) {
		m->odd1 = odd;
		m->team1 = bookie_team_get_id(bk, team);
	} else if (m->odd2 == 0) {
		m->odd2 = odd;
		if (strcmp(team, "X"))
			m->team2 = bookie_team_get_id(bk, team);
	} else if (m->odd3 == 0) {
		m->odd3 = odd;
		m->team2 = bookie_team_get_id(bk, team);
	}
}

static void bb_parse_bets(struct bboo_parser *bp, xmlDocPtr doc, xmlNodePtr cur)
{
	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "table"))
			break;

		cur = cur->next;
	}

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "tbody"))
			break;

		cur = cur->next;
	}

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "tr"))
			break;

		cur = cur->next;
	}

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "td"))
			bb_parse_team(bp, doc, cur);

		cur = cur->next;
	}
}

static void bb_parse_match(struct bboo_parser *bp, xmlDocPtr doc, xmlNodePtr cur)
{
	xmlChar *p, *v;
	struct match *m = bp->m;
	struct betstop *bs = bp->bk->bs;

	m->odd1 = 0;
	m->odd2 = 0;
	m->odd3 = 0;

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		p = NULL;
		if (!xmlStrcmp(cur->name, (const xmlChar *) "td"))
			p = xmlGetProp(cur, (const xmlChar *) "class");

		if (p && !xmlStrcmp(p, (const xmlChar *) "matchHour")) {
			v = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
			if (v) {
				strptime(v, "%H:%M", &bp->tm);
				xmlFree(v);
			}
		} else if (p && !xmlStrcmp(p, (const xmlChar *) "matchBets")) {
			bb_parse_bets(bp, doc, cur);
		}

		if (p)
			xmlFree(p);

		cur = cur->next;
	}

	m->sched = mktime(&bp->tm) - 3600;
	betstop_match_add(bs, m);
}

static void bb_parse_body(struct bboo_parser *bp, xmlDocPtr doc, xmlNodePtr cur)
{
	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "tr"))
			bb_parse_match(bp, doc, cur);

		cur = cur->next;
	}
}

static void bb_parse_table(struct bboo_parser *bp, xmlDocPtr doc, xmlNodePtr cur)
{
	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "thead"))
			bb_parse_date(bp, doc, cur);
		else if (!xmlStrcmp(cur->name, (const xmlChar *) "tbody"))
			bb_parse_body(bp, doc, cur);

		cur = cur->next;
	}
}

static int betboo_parse(struct bookie *bk, const char *fname)
{
	int          fd, ret;
	char         *htmlbuff;
	struct stat  st;
	struct betstop *bs = bk->bs;

	htmlDocPtr        doc;
	xmlNodeSetPtr     nodeset;
	xmlXPathObjectPtr nodes;
	htmlParserCtxtPtr parser;

	bs_info("Parsing bookie file: %s\n", fname);

	ret = stat(fname, &st);
	if (ret != 0) {
		bs_err("File not found: %s\n", fname);
		return BS_EINVAL;
	}

	htmlbuff = malloc(st.st_size);
	if (!htmlbuff) {
		bs_err("Unable to allocate %ld bytes\n", (long int) st.st_size);
		return -BS_ENOMEM;
	}

	fd = open(fname, O_RDONLY);
	if (fd < 0) {
		ret = -BS_ENOENT;
		goto fail_open;
	}

	parser = htmlCreateMemoryParserCtxt(htmlbuff, st.st_size);
	if (!parser) {
		bs_err("Failed to create an html parser context\n");
		ret = -BS_ENOMEM;
		goto fail_ctxt;
	}

	htmlCtxtUseOptions(parser, HTML_PARSE_NOBLANKS | HTML_PARSE_NOERROR |
				   HTML_PARSE_NOWARNING | HTML_PARSE_NONET);

	doc = htmlCtxtReadFd(parser, fd, NULL, NULL, 0);
	if (!doc) {
		bs_err("Failed to open the html document\n");
		ret = -BS_EINVAL;
		goto fail_readfd;
	}

	nodes = betstop_nodeset(doc, (xmlChar*) "//table[@class='matchesTable']");
	if (nodes) {
		int    i;
		struct bboo_parser bp;

		bp.m = &bk->match;
		bp.bk = bk;

		nodeset = nodes->nodesetval;
		for (i = 0; i < nodeset->nodeNr; i++)
			bb_parse_table(&bp, doc, nodeset->nodeTab[i]);

		xmlXPathFreeObject(nodes);
	}

	xmlFreeDoc(doc);

fail_readfd:
	htmlFreeParserCtxt(parser);

fail_ctxt:
	close(fd);

fail_open:
	free(htmlbuff);

	return 0;
}

static int betboo_fetch(struct bookie *bk, const char *url, const char *fname)
{
	int    ret = BS_ENONE;
	struct curl *curl;
	struct receive rcv;

	curl = curl_easy_init();
	if (!curl)
		return -BS_ENOMEM;

	rcv.size = 0;
	rcv.fd = open(fname, O_WRONLY | O_CREAT,
			S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
	if (rcv.fd < 0) {
		ret = -BS_ENOENT;
		goto exit_curl;
	}

	curl_easy_setopt(curl, CURLOPT_URL, url);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, betstop_write);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *) &rcv);
	curl_easy_setopt(curl, CURLOPT_COOKIE, BETBOO_COOKIE);
	curl_easy_setopt(curl, CURLOPT_USERAGENT, BS_DEF_USERAGENT);

	bs_info("Fetching sport data: url(%s) fname(%s)...\n", url, fname);

	ret = curl_easy_perform(curl);
	if(ret != CURLE_OK) {
		bs_err("curl_easy_perform failed (%s)\n",
			    curl_easy_strerror(ret));
		ret = -BS_ENOENT;
	} else {
		bs_info("Fetched %ld bytes\n", rcv.size);
		ret = rcv.size > 0 ? BS_ENONE : -BS_ENOENT;
	}

	close(rcv.fd);

exit_curl:
	curl_easy_cleanup(curl);

	return ret;
}

struct bookie betboo = {
	.fetch = betboo_fetch,
	.parse = betboo_parse,
};

/* End of file */
