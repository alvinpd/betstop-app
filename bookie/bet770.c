/*
 * File:   bet770.c
 * Author: Alvin Difuntorum <alvinpd09@gmail.com>
 *
 *
 */

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "betstop.h"

#define BET770_COOKIE		"setlng=1"

struct b770_parser {
	struct bookie    *bk;
	struct match     *m;
};

static void b770_parse_odds(struct b770_parser *bp, xmlDocPtr doc, xmlNodePtr cur)
{
	char    *s, team1[32], team2[32];
	xmlChar *p, *v;
	xmlNodePtr tmp;
	struct match *m = bp->m;
	struct bookie *bk = bp->bk;

	m->odd1 = 0;
	m->odd2 = 0;
	m->odd3 = 0;

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		tmp = NULL;

		if (!xmlStrcmp(cur->name, (const xmlChar *) "span")) {
			tmp = cur->xmlChildrenNode;
		}

		while (tmp != NULL) {
			if (!xmlStrcmp(tmp->name, (const xmlChar *) "a")) {
				p = xmlGetProp(tmp, "title");
				v = xmlNodeListGetString(doc,
					tmp->xmlChildrenNode, 1);
				if (p && v) {
					if (m->odd1 == 0) {
						s = &team1[0];
						m->odd1 = strtod(v, NULL);
					} else if (m->odd2 == 0) {
						s = &team2[0];
						m->odd2 = strtod(v, NULL);
					} else if (m->odd3 == 0) {
						s = &team2[0];
						m->odd3 = strtod(v, NULL);
					}

					if (s)
						snprintf(s, 32, "%s", p);
				}

				if (p)
					xmlFree(p);
				if (v)
					xmlFree(v);
			}
			tmp = tmp->next;
		}

		cur = cur->next;
	}

	m->team1 = bookie_team_get_id(bk, team1);
	m->team2 = bookie_team_get_id(bk, team2);
}

static void b770_parse_match(struct b770_parser *bp, xmlDocPtr doc, xmlNodePtr cur)
{
	int     date_ok = 0;
	xmlChar *v;
	struct tm tm;
	struct match *m = bp->m;
	struct betstop *bs = bp->bk->bs;

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "span")) {
			if (date_ok)
				break;

			v = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
			if (v) {
				memset(&tm, 0, sizeof(tm));
				tm.tm_isdst = -1;
				strptime(v, "%b %d, %Y - %H:%M", &tm);
				m->sched = mktime(&tm) - 3600;
			}
			date_ok = 1;

			if (v)
				xmlFree(v);
		} else if (!xmlStrcmp(cur->name, (const xmlChar *) "div")) {
			b770_parse_odds(bp, doc, cur);
		}

		cur = cur->next;
	}

	betstop_match_add(bs, m);
}

static int bet770_parse(struct bookie *bk, const char *fname)
{
	int          fd, ret;
	char         *htmlbuff;
	struct stat  st;
	struct betstop *bs = bk->bs;

	htmlDocPtr        doc;
	xmlNodeSetPtr     nodeset;
	xmlXPathObjectPtr nodes;
	htmlParserCtxtPtr parser;

	bs_info("Parsing bookie file: %s\n", fname);

	ret = stat(fname, &st);
	if (ret != 0) {
		bs_err("File not found: %s\n", fname);
		return BS_EINVAL;
	}

	htmlbuff = malloc(st.st_size);
	if (!htmlbuff) {
		bs_err("Unable to allocate %ld bytes\n", (long int) st.st_size);
		return -BS_ENOMEM;
	}

	fd = open(fname, O_RDONLY);
	if (fd < 0) {
		ret = -BS_ENOENT;
		goto fail_open;
	}

	parser = htmlCreateMemoryParserCtxt(htmlbuff, st.st_size);
	if (!parser) {
		bs_err("Failed to create an html parser context\n");
		ret = -BS_ENOMEM;
		goto fail_ctxt;
	}

	htmlCtxtUseOptions(parser, HTML_PARSE_NOBLANKS | HTML_PARSE_NOERROR |
				   HTML_PARSE_NOWARNING | HTML_PARSE_NONET);

	doc = htmlCtxtReadFd(parser, fd, NULL, NULL, 0);
	if (!doc) {
		bs_err("Failed to open the html document\n");
		ret = -BS_EINVAL;
		goto fail_readfd;
	}

	nodes = betstop_nodeset(doc, (xmlChar*)
			"//div[@class='betRow betRow2'] | " \
			"//div[@class='betRow betRow2 betRow3']");
	if (nodes) {
		int    i;
		struct b770_parser bp;

		bp.m = &bk->match;
		bp.bk = bk;

		nodeset = nodes->nodesetval;
		for (i = 0; i < nodeset->nodeNr; i++)
			b770_parse_match(&bp, doc, nodeset->nodeTab[i]);

		xmlXPathFreeObject(nodes);
	}

	xmlFreeDoc(doc);

fail_readfd:
	htmlFreeParserCtxt(parser);

fail_ctxt:
	close(fd);

fail_open:
	free(htmlbuff);

	return 0;
}

static int bet770_fetch(struct bookie *bk, const char *url, const char *fname)
{
	int         ret = BS_ENONE;
	struct curl *curl;
	struct receive rcv;

	curl = curl_easy_init();
	if (!curl)
		return -BS_ENOMEM;

	rcv.size = 0;
	rcv.fd = open(fname, O_WRONLY | O_CREAT,
			S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
	if (rcv.fd < 0) {
		ret = -BS_ENOENT;
		goto exit_curl;
	}

	curl_easy_setopt(curl, CURLOPT_URL, url);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, betstop_write);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *) &rcv);
	curl_easy_setopt(curl, CURLOPT_COOKIE, BET770_COOKIE);
	curl_easy_setopt(curl, CURLOPT_USERAGENT, BS_DEF_USERAGENT);

	bs_info("Fetching sport data: url(%s)...\n", url);

	ret = curl_easy_perform(curl);
	if(ret != CURLE_OK) {
		bs_err("curl_easy_perform failed (%s)\n",
			    curl_easy_strerror(ret));
		ret = -BS_ENOENT;
	} else {
		bs_info("Fetched %ld bytes\n", rcv.size);
		ret = rcv.size > 0 ? BS_ENONE : -BS_ENOENT;
	}

	close(rcv.fd);

exit_curl:
	curl_easy_cleanup(curl);

	return ret;
}

struct bookie bet770 = {
	.fetch = bet770_fetch,
	.parse = bet770_parse,
};

/* End of file */
