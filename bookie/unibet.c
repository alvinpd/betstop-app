/*
 * File:   unibet.c
 * Author: Alvin Difuntorum <alvinpd09@gmail.com>
 *
 *
 */

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "betstop.h"

#define UNIBET_COOKIE "unibet.lang=en_GB"

struct ubet_parser {
	struct tm     tm;
	struct match  *m;
	struct bookie *bk;
};

static void ub_parse_date(struct ubet_parser *up, xmlDocPtr doc, xmlNodePtr cur)
{
	char date[32] = {0, };
	xmlChar *v;

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "td"))
			break;
		cur = cur->next;
	}

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "div"))
			break;
		cur = cur->next;
	}

	v = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
	if (v) {
		memset(&up->tm, 0, sizeof(up->tm));
		up->tm.tm_isdst = -1;
		betstop_str_trim(v, date);
		strptime(date, "%d-%b-%Y", &up->tm);
		xmlFree(v);
	}
}

static void ub_parse_time(struct ubet_parser *up, xmlDocPtr doc, xmlNodePtr cur)
{
	char time[10] = {0, };
	xmlChar *c, *v;

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		c = NULL;
		if (!xmlStrcmp(cur->name, (const xmlChar *) "div"))
			c = xmlGetProp(cur, (const xmlChar *) "class");

		if (c) {
			v = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
			if (v) {
				betstop_str_trim(v, time);
				strptime(time, "%H:%M", &up->tm);
				xmlFree(v);
			}
		}

		if (c)
			xmlFree(c);
		cur = cur->next;
	}
}

static void ub_parse_team(struct ubet_parser *up, xmlDocPtr doc, xmlNodePtr cur)
{
	char team[32];
	xmlChar *c, *v;
	struct match *m = up->m;
	struct bookie *bk = up->bk;

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		c = NULL;
		if (!xmlStrcmp(cur->name, (const xmlChar *) "div"))
			c = xmlGetProp(cur, (const xmlChar *) "class");

		if (c && xmlStrstr(c, "uTdInnerTeam") != NULL) {
			v = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
			if (v) {
				memset(team, 0, ARRAY_SIZE(team));
				betstop_str_trim(v, team);
				if (m->team1 == 0)
					m->team1 = bookie_team_get_id(bk, team);
				else if (m->team2 == 0)
					m->team2 = bookie_team_get_id(bk, team);

				xmlFree(v);
			}
		}

		if (c)
			xmlFree(c);
		cur = cur->next;
	}
}

static void ub_parse_odds(struct ubet_parser *up, xmlDocPtr doc, xmlNodePtr cur)
{
	char    odds[10];
	xmlChar *c, *v;
	struct match *m = up->m;

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		c = NULL;
		if (!xmlStrcmp(cur->name, (const xmlChar *) "div"))
			c = xmlGetProp(cur, (const xmlChar *) "class");

		if (c && xmlStrstr(c, "uTdInner") != NULL) {
			xmlFree(c);
			break;
		}

		cur = cur->next;
	}

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "a")) {
			v = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
			if (v) {
				memset(odds, 0, ARRAY_SIZE(odds));
				betstop_str_trim(v, odds);

				if (m->odd1 == 0)
					m->odd1 = strtod(odds, NULL);
				else if (m->odd2 == 0)
					m->odd2 = strtod(odds, NULL);
				else if (m->odd3 == 0)
					m->odd3 = strtod(odds, NULL);

				xmlFree(v);
			}
		}

		cur = cur->next;
	}
}

static void ub_parse_match(struct ubet_parser *up, xmlDocPtr doc, xmlNodePtr cur)
{
	int td = 0;
	xmlChar *c;
	struct match *m = up->m;
	struct betstop *bs = up->bk->bs;

	m->odd1 = 0;
	m->odd2 = 0;
	m->odd3 = 0;
	m->team1 = 0;
	m->team2 = 0;

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		c = NULL;
		if (!xmlStrcmp(cur->name, (const xmlChar *) "td")) {
			td++;
			c = xmlGetProp(cur, (const xmlChar *) "class");
		}

		if (td && c == NULL) {
			if (td == 1)
				ub_parse_time(up, doc, cur);
			else if (td == 2)
				ub_parse_team(up, doc, cur);
		} else if (c && !xmlStrcmp(c, (const xmlChar *) "uTdInnerBetTd")) {
			ub_parse_odds(up, doc, cur);
		}

		if (c)
			xmlFree(c);
		cur = cur->next;
	}

	m->sched = mktime(&up->tm);
	betstop_match_add(bs, m);
}

static void ub_parse_entry(struct ubet_parser *up, xmlDocPtr doc, xmlNodePtr cur)
{
	xmlChar *p;

	p = xmlGetProp(cur, (const xmlChar *) "id");
	if (!p || (p && xmlStrstr(p, (const xmlChar *) ".threeWay_") == NULL)) {
		if (p)
			xmlFree(p);
		return;
	}

	if (p)
		xmlFree(p);

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "tbody"))
			break;
		cur = cur->next;
	}

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		p = NULL;
		if (!xmlStrcmp(cur->name, (const xmlChar *) "tr"))
			p = xmlGetProp(cur, (const xmlChar *) "class");

		if (p && !xmlStrcmp(p, (const xmlChar *) "uTrLevel3"))
			ub_parse_date(up, doc, cur);
		else if (p && !xmlStrcmp(p, (const xmlChar *) "uTrLevel5"))
			ub_parse_match(up, doc, cur);

		if (p)
			xmlFree(p);
		cur = cur->next;
	}
}

static int unibet_parse(struct bookie *bk, const char *fname)
{
	int          fd, ret;
	char         *htmlbuff;
	struct stat  st;

	htmlDocPtr        doc;
	xmlNodeSetPtr     nodeset;
	xmlXPathObjectPtr nodes;
	htmlParserCtxtPtr parser;

	bs_info("Parsing bookie file: %s\n", fname);

	ret = stat(fname, &st);
	if (ret != 0) {
		bs_err("File not found: %s\n", fname);
		return BS_EINVAL;
	}

	htmlbuff = malloc(st.st_size);
	if (!htmlbuff) {
		bs_err("Unable to allocate %lu bytes\n", (u64) st.st_size);
		return -BS_ENOMEM;
	}

	fd = open(fname, O_RDONLY);
	if (fd < 0) {
		ret = -BS_ENOENT;
		goto fail_open;
	}

	parser = htmlCreateMemoryParserCtxt(htmlbuff, st.st_size);
	if (!parser) {
		bs_err("Failed to create an html parser context\n");
		ret = -BS_ENOMEM;
		goto fail_ctxt;
	}

	htmlCtxtUseOptions(parser, HTML_PARSE_NOBLANKS | HTML_PARSE_NOERROR |
				   HTML_PARSE_NOWARNING | HTML_PARSE_NONET);

	doc = htmlCtxtReadFd(parser, fd, NULL, NULL, 0);
	if (!doc) {
		bs_err("Failed to open the html document\n");
		ret = -BS_EINVAL;
		goto fail_readfd;
	}

	nodes = betstop_nodeset(doc, (xmlChar*) "//table[@class='uTableBetOffer']");
	if (nodes) {
		int    i;
		struct ubet_parser up;

		up.m = &bk->match;
		up.bk = bk;

		nodeset = nodes->nodesetval;
		for (i = 0; i < nodeset->nodeNr; i++)
			ub_parse_entry(&up, doc, nodeset->nodeTab[i]);

		xmlXPathFreeObject(nodes);
	}

	xmlFreeDoc(doc);

	fail_readfd:
	htmlFreeParserCtxt(parser);

fail_ctxt:
	close(fd);

fail_open:
	free(htmlbuff);

	return ret;
}

static int unibet_fetch(struct bookie *bk, const char *url, const char *fname)
{
	int         ret = BS_ENONE;
	struct curl *curl;
	struct receive rcv;

	curl = curl_easy_init();
	if (!curl)
		return -BS_ENOMEM;

	rcv.size = 0;
	rcv.fd = open(fname, O_WRONLY | O_CREAT,
			S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
	if (rcv.fd < 0) {
		ret = -BS_ENOENT;
		goto exit_curl;
	}

	curl_easy_setopt(curl, CURLOPT_URL, url);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, betstop_write);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *) &rcv);
	curl_easy_setopt(curl, CURLOPT_COOKIE, UNIBET_COOKIE);
	curl_easy_setopt(curl, CURLOPT_USERAGENT, BS_DEF_USERAGENT);

	bs_info("Fetching sport data: url(%s) fname(%s)...\n", url, fname);

	ret = curl_easy_perform(curl);
	if(ret != CURLE_OK) {
		bs_err("curl_easy_perform failed (%s)\n",
			    curl_easy_strerror(ret));
		ret = -BS_ENOENT;
	} else {
		bs_info("Fetched %ld bytes\n", rcv.size);
		ret = rcv.size > 0 ? BS_ENONE : -BS_ENOENT;
	}

	close(rcv.fd);

exit_curl:
	curl_easy_cleanup(curl);

	return ret;
}

struct bookie unibet = {
	.fetch = unibet_fetch,
	.parse = unibet_parse,
};

/* End of file */
