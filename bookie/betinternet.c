/*
 * File:   betinternet.c
 * Author: Alvin Difuntorum <alvinpd09@gmail.com>
 *
 *
 */

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "betstop.h"

#define BETINTERNET_COOKIE	""

struct bi_parser {
	struct match  *m;
	struct bookie *bk;
};

static void bi_parse_team(struct bi_parser *bi, const xmlChar *v)
{
	char *s, *e;
	char team[64] = {0, };
	struct match *m = bi->m;
	struct bookie *bk = bi->bk;

	s = (char *)v;
	e = strstr((const char *) v, " v ");
	memcpy(&team[0], s, e - s);
	m->team1 = bookie_team_get_id(bk, team);
	s = e + 3;
	m->team2 = bookie_team_get_id(bk, s);
}

static void bi_parse_match(struct bi_parser *bi, xmlDocPtr doc, xmlNodePtr cur)
{
	xmlChar *p, *v;
	xmlNodePtr tmp;
	struct tm tm;
	struct match *m = bi->m;

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		p = NULL;
		if (!xmlStrcmp(cur->name, (const xmlChar *) "span"))
			p = xmlGetProp(cur, (const xmlChar *) "class");

		if (p && !xmlStrcmp(p, (const xmlChar *) "couponMatch")) {
			v = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
			if (v) {
				bi_parse_team(bi, v);
				xmlFree(v);
			}
		} else if (p && !xmlStrcmp(p, (const xmlChar *) "couponDate")) {
			v = NULL;
			tmp = cur->xmlChildrenNode;
			while (tmp != NULL) {
				if (!xmlStrcmp(tmp->name,
					(const xmlChar *) "span")) {
					v = xmlNodeListGetString(doc,
						tmp->xmlChildrenNode, 1);
					break;
				}
				tmp = tmp->next;
			}

			if (v) {
				strptime(v, "%Y-%m-%dT%H:%M:%S", &tm);
				tm.tm_sec = 0;
				tm.tm_isdst = -1;
				m->sched = mktime(&tm);
				xmlFree(v);
			}
		}

		if (p)
			xmlFree(p);

		cur = cur->next;
	}
}

static void bi_parse_entry(struct bi_parser *bi, xmlDocPtr doc, xmlNodePtr cur)
{
	xmlChar *p, *v;
	xmlNodePtr tmp;
	struct match *m = bi->m;
	struct betstop *bs = bi->bk->bs;

	m->odd1 = 0;
	m->odd2 = 0;
	m->odd3 = 0;

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		p = NULL;
		if (!xmlStrcmp(cur->name, (const xmlChar *) "td"))
			p = xmlGetProp(cur, (const xmlChar *) "class");

		if (p && !xmlStrcmp(p, (const xmlChar *) "match")) {
			bi_parse_match(bi, doc, cur);
		} else if (p && !xmlStrcmp(p, (const xmlChar *) "odd")) {
			tmp = cur->xmlChildrenNode;
			while (tmp != NULL) {
				v = NULL;
				if (!xmlStrcmp(tmp->name, (const xmlChar *) "a")) {
					v = xmlNodeListGetString(doc,
						tmp->xmlChildrenNode, 1);
				}

				if (v) {
					if (m->odd1 == 0)
						m->odd1 = strtod(v, NULL);
					else if (m->odd2 == 0)
						m->odd2 = strtod(v, NULL);
					else if (m->odd3 == 0)
						m->odd3 = strtod(v, NULL);
					xmlFree(v);
				}

				tmp = tmp->next;
			}
		}

		if (p)
			xmlFree(p);

		cur = cur->next;
	}

	betstop_match_add(bs, m);
}

int betinternet_parse(struct bookie *bk, const char *fname)
{
	int          fd, ret;
	char         *htmlbuff;
	struct stat  st;
	struct betstop *bs = bk->bs;

	htmlDocPtr        doc;
	xmlNodeSetPtr     nodeset;
	xmlXPathObjectPtr nodes;
	htmlParserCtxtPtr parser;

	bs_info("Parsing bookie file: %s\n", fname);

	ret = stat(fname, &st);
	if (ret != 0) {
		bs_err("File not found: %s\n", fname);
		return BS_EINVAL;
	}

	htmlbuff = malloc(st.st_size);
	if (!htmlbuff) {
		bs_err("Unable to allocate %ld bytes\n", (long int) st.st_size);
		return -BS_ENOMEM;
	}

	fd = open(fname, O_RDONLY);
	if (fd < 0) {
		ret = -BS_ENOENT;
		goto fail_open;
	}

	parser = htmlCreateMemoryParserCtxt(htmlbuff, st.st_size);
	if (!parser) {
		bs_err("Failed to create an html parser context\n");
		ret = -BS_ENOMEM;
		goto fail_ctxt;
	}

	htmlCtxtUseOptions(parser, HTML_PARSE_NOBLANKS | HTML_PARSE_NOERROR |
				   HTML_PARSE_NOWARNING | HTML_PARSE_NONET);

	doc = htmlCtxtReadFd(parser, fd, NULL, NULL, 0);
	if (!doc) {
		bs_err("Failed to open the html document\n");
		ret = -BS_EINVAL;
		goto fail_readfd;
	}

	nodes = betstop_nodeset(doc, (xmlChar*) "//tr[@class='one'] | " \
						"//tr[@class='two']");
	if (nodes) {
		int    i;
		struct bi_parser bp;

		bp.m = &bk->match;
		bp.bk = bk;

		nodeset = nodes->nodesetval;
		for (i = 0; i < nodeset->nodeNr; i++)
			bi_parse_entry(&bp, doc, nodeset->nodeTab[i]);

		xmlXPathFreeObject(nodes);
	}

	xmlFreeDoc(doc);

fail_readfd:
	htmlFreeParserCtxt(parser);

fail_ctxt:
	close(fd);

fail_open:
	free(htmlbuff);

	return 0;
}

int betinternet_fetch(struct bookie *bk, const char *url, const char *fname)
{
	int    ret = BS_ENONE;
	struct curl *curl;
	struct receive rcv;

	curl = curl_easy_init();
	if (!curl)
		return -BS_ENOMEM;

	rcv.size = 0;
	rcv.fd = open(fname, O_WRONLY | O_CREAT,
			S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
	if (rcv.fd < 0) {
		ret = -BS_ENOENT;
		goto exit_curl;
	}

	curl_easy_setopt(curl, CURLOPT_URL, url);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, betstop_write);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *) &rcv);
	curl_easy_setopt(curl, CURLOPT_COOKIE, BETINTERNET_COOKIE);
	curl_easy_setopt(curl, CURLOPT_USERAGENT, BS_DEF_USERAGENT);

	bs_info("Fetching sport data: url(%s) fname(%s)...\n", url, fname);

	ret = curl_easy_perform(curl);
	if(ret != CURLE_OK) {
		bs_err("curl_easy_perform failed (%s)\n",
			    curl_easy_strerror(ret));
		ret = -BS_ENOENT;
	} else {
		bs_info("Fetched %ld bytes\n", rcv.size);
		ret = rcv.size > 0 ? BS_ENONE : -BS_ENOENT;
	}

	close(rcv.fd);

exit_curl:
	curl_easy_cleanup(curl);

	return ret;
}

struct bookie betinternet = {
	.fetch = betinternet_fetch,
	.parse = betinternet_parse,
};

/* End of file */
