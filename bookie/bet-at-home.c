/*
 * File:   bet-at-home.c
 * Author: Alvin Difuntorum <alvinpd09@gmail.com>
 *
 *
 */

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "betstop.h"

#define BAH_COOKIE	"BAHLang=EN; CustomEventGroupIDLang=197EN100Normal-1"

struct bah_parser {
	struct match  *m;
	struct bookie *bk;
};

static void bah_str_trim(const char *in, char *out)
{
	char *start, *end;

	start = (char *)in;

	while (*start++ < 0x21);
	start--;

	end = (char*)(start + strlen(start));
	while (*end-- < 0x21);

	memcpy(out, start, end - start + 2);
}

static void bah_parse_teams(struct bah_parser *bp, const char *str)
{
	char team[32] = {0, }, *s;
	struct match *m = bp->m;
	struct bookie *bk = bp->bk;

	s = strstr(str, " - ");
	if (!s)
		return;

	memcpy(team, str, s - str);
	m->team1 = bookie_team_get_id(bk, team);

	s += 3;
	memset(team, 0, ARRAY_SIZE(team));
	bah_str_trim(s, team);

	m->team2 = bookie_team_get_id(bk, team);
}

static void bah_parse_match(struct bah_parser *bp, xmlDocPtr doc, xmlNodePtr cur)
{
	int    i, tmok = 0;
	char   odd[10], *s;
	xmlChar *c, *cs, *v;
	struct tm tm;
	struct match *m = bp->m;
	struct betstop *bs = bp->bk->bs;

	m->odd1 = 0;
	m->odd2 = 0;
	m->odd3 = 0;

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		c = NULL;
		cs = NULL;
		if (!xmlStrcmp(cur->name, (const xmlChar *) "td")) {
			c = xmlGetProp(cur, (const xmlChar *) "class");
			cs = xmlGetProp(cur, (const xmlChar *) "colspan");
		}

		if (cs && !xmlStrcmp(cs, (const xmlChar *) "2")) {
			v = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
			if (v) {
				tmok = 1;
				bah_parse_teams(bp, v);
				xmlFree(v);
			}
		}

		if (c && !xmlStrcmp(c, (const xmlChar *) "OT_Col_FontSmall_BR")) {
			memset(&tm, 0, sizeof(tm));
			v = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
			if (v) {
				tm.tm_isdst = -1;
				strptime(v, "%d/%m/%y %H:%M", &tm);
				xmlFree(v);
			}
		} else if (c && !xmlStrcmp(c, (const xmlChar *) "OT_Col_Odd_BR")) {
			v = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
			if (v) {
				memset(&odd[0], 0, ARRAY_SIZE(odd));
				bah_str_trim(v, odd);

				s = strstr(odd, ",");
				if (s)
					*s = '.';

				if (m->odd1 == 0)
					m->odd1 = strtod(odd, NULL);
				else if (m->odd2 == 0)
					m->odd2 = strtod(odd, NULL);
				else if (m->odd3 == 0)
					m->odd3 = strtod(odd, NULL);

				xmlFree(v);
			}
		}

		if (c)
			xmlFree(c);
		if (cs)
			xmlFree(cs);

		cur = cur->next;
	}

	if (!tmok)
		return;

	m->sched = mktime(&tm) - 3600;
	betstop_match_add(bs, m);
}

static int bet_at_home_parse(struct bookie *bk, const char *fname)
{
	int          fd, ret;
	char         *htmlbuff;
	struct stat  st;
	struct betstop *bs = bk->bs;

	htmlDocPtr        doc;
	xmlNodeSetPtr     nodeset;
	xmlXPathObjectPtr nodes;
	htmlParserCtxtPtr parser;

	bs_info("Parsing bookie file: %s\n", fname);

	ret = stat(fname, &st);
	if (ret != 0) {
		bs_err("File not found: %s\n", fname);
		return BS_EINVAL;
	}

	htmlbuff = malloc(st.st_size);
	if (!htmlbuff) {
		bs_err("Unable to allocate %ld bytes\n", (long int) st.st_size);
		return -BS_ENOMEM;
	}

	fd = open(fname, O_RDONLY);
	if (fd < 0) {
		ret = -BS_ENOENT;
		goto fail_open;
	}

	parser = htmlCreateMemoryParserCtxt(htmlbuff, st.st_size);
	if (!parser) {
		bs_err("Failed to create an html parser context\n");
		ret = -BS_ENOMEM;
		goto fail_ctxt;
	}

	htmlCtxtUseOptions(parser, HTML_PARSE_NOBLANKS | HTML_PARSE_NOERROR |
				   HTML_PARSE_NOWARNING | HTML_PARSE_NONET);

	doc = htmlCtxtReadFd(parser, fd, NULL, NULL, 0);
	if (!doc) {
		bs_err("Failed to open the html document\n");
		ret = -BS_EINVAL;
		goto fail_readfd;
	}

	nodes = betstop_nodeset(doc, (xmlChar*)
			"//table[@class='OT']/tr[@class='OT_R'] | " \
			"//table[@class='OT']/tr[@class='OT_R_Alt']");
	if (nodes) {
		int    i;
		struct bah_parser bp;

		bp.m = &bk->match;
		bp.bk = bk;

		nodeset = nodes->nodesetval;

		for (i = 0; i < nodeset->nodeNr; i++)
			bah_parse_match(&bp, doc, nodeset->nodeTab[i]);

		xmlXPathFreeObject(nodes);
	}

	xmlFreeDoc(doc);

fail_readfd:
	htmlFreeParserCtxt(parser);

fail_ctxt:
	close(fd);

fail_open:
	free(htmlbuff);

	return 0;
}

static int bet_at_home_fetch(struct bookie *bk, const char *url, const char *fname)
{
	int         ret = BS_ENONE;
	struct curl *curl;
	struct receive rcv;

	struct curl_httppost *formpost = NULL;
	struct curl_httppost *lastptr = NULL;

	curl = curl_easy_init();
	if (!curl)
		return -BS_ENOMEM;

	rcv.size = 0;
	rcv.fd = open(fname, O_WRONLY | O_CREAT,
			S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
	if (rcv.fd < 0) {
		ret = -BS_ENOENT;
		goto exit_curl;
	}

	curl_formadd(&formpost, &lastptr,
		CURLFORM_COPYNAME, "action",
		CURLFORM_COPYCONTENTS, "toggleEventGroup", CURLFORM_END);

	curl_easy_setopt(curl, CURLOPT_URL, url);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, betstop_write);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *) &rcv);
	curl_easy_setopt(curl, CURLOPT_COOKIE, BAH_COOKIE);
	curl_easy_setopt(curl, CURLOPT_USERAGENT, BS_DEF_USERAGENT);
	curl_easy_setopt(curl, CURLOPT_HTTPPOST, formpost);

	bs_info("Fetching sport data: url(%s) fname(%s)...\n", url, fname);

	ret = curl_easy_perform(curl);
	if(ret != CURLE_OK) {
		bs_err("curl_easy_perform failed (%s)\n",
			    curl_easy_strerror(ret));
		ret = -BS_ENOENT;
	} else {
		bs_info("Fetched %ld bytes\n", rcv.size);
		ret = rcv.size > 0 ? BS_ENONE : -BS_ENOENT;
	}

	close(rcv.fd);

exit_curl:
	curl_easy_cleanup(curl);

	return ret;
}

struct bookie bet_at_home = {
	.fetch = bet_at_home_fetch,
	.parse = bet_at_home_parse,
};

/* End of file */
