/*
 * File:   bwin.c
 * Author: Alvin Difuntorum <alvinpd09@gmail.com>
 *
 *
 */


#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "betstop.h"

#define BWIN_COOKIE	"DisplayOddsFormat=EU"

struct bwin_parser {
	struct tm     tm;
	struct match  *m;
	struct bookie *bk;
};

static void bw_parse_date(struct bwin_parser *bp, xmlDocPtr doc, xmlNodePtr cur)
{
	xmlChar *v;
	struct match *m = bp->m;
	struct bookie *bk = bp->bk;

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "span"))
			break;
		cur = cur->next;
	}

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "span"))
			break;
		cur = cur->next;
	}

	v = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
	if (v) {
		memset(&bp->tm, 0, sizeof(bp->tm));
		bp->tm.tm_isdst = -1;
		strptime(v, "%a, %b %d, %Y", &bp->tm);
		xmlFree(v);
	}
}

static void bw_parse_str(const char *in, char *out)
{
	char *start, *end;

	start = (char *)in;

	while (*start++ < 0x21);
	start--;

	end = (char*)(start + strlen(start));
	while (*end-- < 0x21);

	memcpy(out, start, end - start + 2);
}

static void bw_parse_bets(struct bwin_parser *bp, xmlDocPtr doc, xmlNodePtr cur)
{
	char    team[64];
	xmlChar *c, *v;
	struct match  *m = bp->m;
	struct bookie *bk = bp->bk;

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "table"))
			break;
		cur = cur->next;
	}

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "tr"))
			break;
		cur = cur->next;
	}

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		c = NULL;
		if (!xmlStrcmp(cur->name, (const xmlChar *) "td"))
			c = xmlGetProp(cur, (const xmlChar *) "class");

		if (c && !xmlStrcmp(c, (const xmlChar *) "label")) {
			v = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
			if (v) {
				memset(&team[0], 0, ARRAY_SIZE(team));
				bw_parse_str(v, team);
				if (strcmp(team, "X")) {
					if (m->team1 == 0)
						m->team1 = bookie_team_get_id(
								bk, team);
					else
						m->team2 = bookie_team_get_id(
								bk, team);
				}

				xmlFree(v);
			}
		} else if (c && !xmlStrcmp(c, (const xmlChar *) "odd")) {
			v = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
			if (v) {
				if (m->odd1 == 0)
					m->odd1 = strtod(v, NULL);
				else if (m->odd2 == 0)
					m->odd2 = strtod(v, NULL);
				else if (m->odd3 == 0)
					m->odd3 = strtod(v, NULL);

				xmlFree(v);
			}
		}

		if (c)
			xmlFree(c);
		cur = cur->next;
	}
}

static void bw_parse_match(struct bwin_parser *bp, xmlDocPtr doc, xmlNodePtr cur)
{
	char time[10];
	xmlChar *c, *v;
	struct match   *m = bp->m;
	struct bookie  *bk = bp->bk;
	struct betstop *bs = bp->bk->bs;

	m->odd1 = 0;
	m->odd2 = 0;
	m->odd3 = 0;
	m->team1 = 0;
	m->team2 = 0;

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		c = NULL;
		if (!xmlStrcmp(cur->name, (const xmlChar *) "td"))
			c = xmlGetProp(cur, (const xmlChar *) "class");

		if (c && !xmlStrcmp(c, (const xmlChar *) "leftcell minwidth")) {
			v = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
			if (v) {
				bw_parse_str(v, time);
				strptime(time, "%I:%M %p", &bp->tm);
				xmlFree(v);
			}
		} else if (c && !xmlStrcmp(c, (const xmlChar *) "rightcell minwidth")) {
			break;
		} else if (c) {
			bw_parse_bets(bp, doc, cur);
		}

		if (c)
			xmlFree(c);

		cur = cur->next;
	}

	m->sched = mktime(&bp->tm) - 3600;

	betstop_match_add(bs, m);
}

static void bw_parse_content(struct bwin_parser *bp, xmlDocPtr doc, xmlNodePtr cur)
{
	xmlChar *c;

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "table"))
			break;
		cur = cur->next;
	}

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		c = NULL;
		if (!xmlStrcmp(cur->name, (const xmlChar *) "tr"))
			c = xmlGetProp(cur, (const xmlChar *) "class");

		if (c && !xmlStrcmp(c, (const xmlChar *) "normal"))
			bw_parse_match(bp, doc, cur);

		if (c)
			xmlFree(c);
		cur = cur->next;
	}
}

static void bw_parse_entry(struct bwin_parser *bp, xmlDocPtr doc, xmlNodePtr cur)
{
	xmlChar *c;

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		c = NULL;
		if (!xmlStrcmp(cur->name, (const xmlChar *) "div"))
			c = xmlGetProp(cur, (const xmlChar *) "class");


		if (c && (!xmlStrcmp(c, (const xmlChar *) "controlHeaderNoShadow") ||
			  !xmlStrcmp(c, (const xmlChar *) "controlHeader")))
			bw_parse_date(bp, doc, cur);
		else if (c && !xmlStrcmp(c, (const xmlChar *) "ControlContent"))
			bw_parse_content(bp, doc, cur);

		if (c)
			xmlFree(c);
		cur = cur->next;
	}
}

static int bwin_parse(struct bookie *bk, const char *fname)
{
	int          fd, ret;
	char         *htmlbuff;
	struct stat  st;
	struct betstop *bs = bk->bs;

	htmlDocPtr        doc;
	xmlNodeSetPtr     nodeset;
	xmlXPathObjectPtr nodes;
	htmlParserCtxtPtr parser;

	bs_info("Parsing bookie file: %s\n", fname);

	ret = stat(fname, &st);
	if (ret != 0) {
		bs_err("File not found: %s\n", fname);
		return BS_EINVAL;
	}

	htmlbuff = malloc(st.st_size);
	if (!htmlbuff) {
		bs_err("Unable to allocate %ld bytes\n", (long int) st.st_size);
		return -BS_ENOMEM;
	}

	fd = open(fname, O_RDONLY);
	if (fd < 0) {
		ret = -BS_ENOENT;
		goto fail_open;
	}

	parser = htmlCreateMemoryParserCtxt(htmlbuff, st.st_size);
	if (!parser) {
		bs_err("Failed to create an html parser context\n");
		ret = -BS_ENOMEM;
		goto fail_ctxt;
	}

	htmlCtxtUseOptions(parser, HTML_PARSE_NOBLANKS | HTML_PARSE_NOERROR |
				   HTML_PARSE_NOWARNING | HTML_PARSE_NONET);

	doc = htmlCtxtReadFd(parser, fd, NULL, NULL, 0);
	if (!doc) {
		bs_err("Failed to open the html document\n");
		ret = -BS_EINVAL;
		goto fail_readfd;
	}

	nodes = betstop_nodeset(doc, (xmlChar*) "//div[@id='ctl01_ctl09_ctl03']");
	if (nodes) {
		int    i;
		struct bwin_parser bp;

		bp.m = &bk->match;
		bp.bk = bk;

		nodeset = nodes->nodesetval;

		for (i = 0; i < nodeset->nodeNr; i++)
			bw_parse_entry(&bp, doc, nodeset->nodeTab[i]);

		xmlXPathFreeObject(nodes);
	}

	xmlFreeDoc(doc);

fail_readfd:
	htmlFreeParserCtxt(parser);

fail_ctxt:
	close(fd);

fail_open:
	free(htmlbuff);

	return 0;
}

static int bwin_fetch(struct bookie *bk, const char *url, const char *fname)
{
	int         ret = BS_ENONE;
	struct curl *curl;
	struct receive rcv;

	curl = curl_easy_init();
	if (!curl)
		return -BS_ENOMEM;

	rcv.size = 0;
	rcv.fd = open(fname, O_WRONLY | O_CREAT,
			S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
	if (rcv.fd < 0) {
		ret = -BS_ENOENT;
		goto exit_curl;
	}

	curl_easy_setopt(curl, CURLOPT_URL, url);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, betstop_write);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *) &rcv);
	curl_easy_setopt(curl, CURLOPT_COOKIE, BWIN_COOKIE);
	curl_easy_setopt(curl, CURLOPT_USERAGENT, BS_DEF_USERAGENT);

	bs_info("Fetching sport data: url(%s) fname(%s)...\n", url, fname);

	ret = curl_easy_perform(curl);
	if(ret != CURLE_OK) {
		bs_err("curl_easy_perform failed (%s)\n",
			    curl_easy_strerror(ret));
		ret = -BS_ENOENT;
	} else {
		bs_info("Fetched %ld bytes\n", rcv.size);
		ret = rcv.size > 0 ? BS_ENONE : -BS_ENOENT;
	}

	close(rcv.fd);

exit_curl:
	curl_easy_cleanup(curl);

	return ret;
}

struct bookie bwin = {
	.fetch = bwin_fetch,
	.parse = bwin_parse,
};

/* End of file */
