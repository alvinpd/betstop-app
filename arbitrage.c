/*
 * File:   arbitrage.c
 * Author: Alvin Difuntorum <alvinpd09@gmail.com>
 *
 *
 */

#include <stdio.h>
#include <stdlib.h>

#include "betstop.h"

struct srch_str {
	int  len;
	char *str;
};

static struct competition *comp_find(struct list_head *lh, struct competition c)
{
	struct competition *a, *b;

	list_for_each_entry_safe(a, b, lh, list) {
		if (a->league == c.league && a->sched == c.sched &&
		    a->team1 == c.team1 && a->team2 == c.team2) {
			return a;
		}
	}

	return NULL;
}

static int cbk_query_matches(void *list, int argc, char **argv, char **col)
{
	int i;
	struct odd *o;
	struct list_head *lh = list;
	struct competition *c, comp;

	o = malloc(sizeof(*o));
	if (o == NULL)
		return -SQLITE_NOMEM;

	INIT_LIST_HEAD(&o->list);
	for (i = 0; i < argc; i++) {
		if (!strcmp(col[i], "league_id"))
			comp.league = strtoul(argv[i], NULL, 10);
		else if (!strcmp(col[i], "team1_id"))
			comp.team1 = strtoul(argv[i], NULL, 10);
		else if (!strcmp(col[i], "team2_id"))
			comp.team2 = strtoul(argv[i], NULL, 10);
		else if (!strcmp(col[i], "sched"))
			comp.sched = strtoul(argv[i], NULL, 10);
		else if (!strcmp(col[i], "bookie_id"))
			o->bookie = strtoul(argv[i], NULL, 10);
		else if (!strcmp(col[i], "odd1"))
			o->odd1 = strtod(argv[i], NULL);
		else if (!strcmp(col[i], "odd2"))
			o->odd2 = strtod(argv[i], NULL);
		else if (!strcmp(col[i], "odd3"))
			o->odd3 = strtod(argv[i], NULL);
	}

	c = comp_find(lh, comp);
	if (c == NULL) {
		c = malloc(sizeof(*c));
		if (c == NULL) {
			free(o);
			return -SQLITE_NOMEM;
		}

		c->arbs = NULL;
		c->count = 0;
		c->arb_total = 0;
		INIT_LIST_HEAD(&c->list);
		INIT_LIST_HEAD(&c->odds);

		c->league = comp.league;
		c->team1 = comp.team1;
		c->team2 = comp.team2;
		c->sched = comp.sched;

		list_add_tail(&c->list, lh);
	}

	c->count++;
	list_add_tail(&o->list, &c->odds);

	return 0;
}

static inline struct odd *odd_find(struct list_head *lh, int idx)
{
	int i = 0;
	struct odd *odd, *o;

	list_for_each_entry_safe(odd, o, lh, list) {
		if (i++ == idx)
			return odd;
	}

	return NULL;
}

static inline double calc_arb(double odd1, double odd2, double odd3)
{
	double r1 = 100.0 / odd1;
	double r2 = 100.0 / odd2;
	double r3 = 100.0 / odd3;
	double total = r1 + r2 + r3;

	return ((100.0 - total) / total) * 100.0;
}

static int cbk_query_name(void *s, int argc, char **argv, char **col)
{
	struct srch_str *str = s;

	if (argc > 0 && !strcmp(col[0], "name"))
		snprintf(str->str, str->len, "%s", argv[0]);

	return 0;
}

static void bs_arb_show(struct betstop *bs, struct competition *comp, FILE *fp)
{
	int  j, i, ret;
	char *msg;
	char q[1024];
	char reg[32], leg[64];
	char team1[64], team2[64];
	char bk1[32], bk2[32], bk3[32];
	double arbval;
	struct srch_str str;
	struct arbitrage *a;

	/* Get Team information */
	str.str = &team1[0];
	str.len = 64;
	sqlite3_snprintf(ARRAY_SIZE(q), q, "SELECT name FROM `teams` "\
			"WHERE `id`=%ld", comp->team1);
	ret = sqlite3_exec(bs->db, q, cbk_query_name, &str, &msg);
	if (ret != SQLITE_OK) {
		bs_err("Query failed: %s\nQuery:\n%s\n", msg, q);
		sqlite3_free(msg);
		return;
	}

	str.str = &team2[0];
	sqlite3_snprintf(ARRAY_SIZE(q), q, "SELECT name FROM `teams` "\
			"WHERE `id`=%ld", comp->team2);
	ret = sqlite3_exec(bs->db, q, cbk_query_name, &str, &msg);
	if (ret != SQLITE_OK) {
		bs_err("Query failed: %s\nQuery:\n%s\n", msg, q);
		sqlite3_free(msg);
		return;
	}

	/* Get Region and League information */
	str.str = &reg[0];
	str.len = 32;
	sqlite3_snprintf(ARRAY_SIZE(q), q, "SELECT region AS name FROM `leagues` "\
			"WHERE `id`=%ld", comp->league);
	ret = sqlite3_exec(bs->db, q, cbk_query_name, &str, &msg);
	if (ret != SQLITE_OK) {
		bs_err("Query failed: %s\nQuery:\n%s\n", msg, q);
		sqlite3_free(msg);
		return;
	}

	str.str = &leg[0];
	str.len = 64;
	sqlite3_snprintf(ARRAY_SIZE(q), q, "SELECT league AS name FROM `leagues` "\
			"WHERE `id`=%ld", comp->league);
	ret = sqlite3_exec(bs->db, q, cbk_query_name, &str, &msg);
	if (ret != SQLITE_OK) {
		bs_err("Query failed: %s\nQuery:\n%s\n", msg, q);
		sqlite3_free(msg);
		return;
	}

	fprintf(fp, "Match: (%s - %s) - (%s v %s) - %s",
		reg, leg, team1, team2, ctime(&comp->sched));

	for (i = 0, j = 0; i < comp->arb_total; i++) {
		a = &comp->arbs[i];

		str.str = &bk1[0];
		str.len = 32;
		sqlite3_snprintf(ARRAY_SIZE(q), q, "SELECT name FROM `bookies` "\
				"WHERE `id`=%ld", a->arb1->bookie);
		ret = sqlite3_exec(bs->db, q, cbk_query_name, &str, &msg);
		if (ret != SQLITE_OK) {
			bs_err("Query failed: %s\nQuery:\n%s\n", msg, q);
			sqlite3_free(msg);
			return;
		}

		str.str = &bk2[0];
		sqlite3_snprintf(ARRAY_SIZE(q), q, "SELECT name FROM `bookies` "\
				"WHERE `id`=%ld", a->arb2->bookie);
		ret = sqlite3_exec(bs->db, q, cbk_query_name, &str, &msg);
		if (ret != SQLITE_OK) {
			bs_err("Query failed: %s\nQuery:\n%s\n", msg, q);
			sqlite3_free(msg);
			return;
		}

		str.str = &bk3[0];
		sqlite3_snprintf(ARRAY_SIZE(q), q, "SELECT name FROM `bookies` "\
				"WHERE `id`=%ld", a->arb3->bookie);
		ret = sqlite3_exec(bs->db, q, cbk_query_name, &str, &msg);
		if (ret != SQLITE_OK) {
			bs_err("Query failed: %s\nQuery:\n%s\n", msg, q);
			sqlite3_free(msg);
			return;
		}

		arbval = calc_arb(a->arb1->odd1, a->arb2->odd2, a->arb3->odd3);
		if (arbval > 0.0) {
			fprintf(fp, "  %3d: %5.02f * %-16s %.02f * %-16s %.02f * %-16s %.02f\n",
				j++, arbval, bk1, a->arb1->odd1, bk2, a->arb2->odd2,
				bk3, a->arb3->odd3);
		}
	}

	if (j == 0)
		fprintf(fp, "  -- None Found --\n");

	fprintf(fp, "\n");
}

int betstop_arb_gen(struct betstop *bs)
{
	int  wrfl = 1;
	int  i, ret = 0, odd1, odd2, odd3;
	char q[1024], *err_msg;
	FILE *fp;
	struct competition *comp, *c;

	fp = fopen("arbs.txt", "wb");
	if (fp == NULL) {
		fp = stdout;
		wrfl = 0;
	}

	sqlite3_snprintf(ARRAY_SIZE(q), q, "SELECT * FROM `matches` "\
			"ORDER BY sched ASC");
	ret = sqlite3_exec(bs->db, q, cbk_query_matches,
			   &bs->competitions, &err_msg);
	if (ret != SQLITE_OK) {
		bs_err("Query failed: %s\nQuery:\n%s\n", err_msg, q);
		sqlite3_free(err_msg);
		if (wrfl)
			fclose(fp);
		return ret;
	}

	list_for_each_entry_safe(comp, c, &bs->competitions, list) {
		if (comp->count < 3)
			continue;

		comp->arb_total =
			(comp->count - 2) * (comp->count - 1) * comp->count;
		comp->arbs = malloc(sizeof(struct arbitrage) * comp->arb_total);
		if (comp->arbs == NULL)
			continue;

		i = 0;

		for (odd1 = 0; odd1 < comp->count; odd1++) {
			for (odd2 = 0; odd2 < comp->count; odd2++) {
				if (odd1 == odd2)
					continue;

				for (odd3 = 0; odd3 < comp->count; odd3++) {
					if (odd1 == odd3 || odd2 == odd3)
						continue;

					comp->arbs[i].arb1 =
						odd_find(&comp->odds, odd1);
					comp->arbs[i].arb2 =
						odd_find(&comp->odds, odd2);
					comp->arbs[i].arb3 =
						odd_find(&comp->odds, odd3);

					i++;
				}
			}
		}

		bs_arb_show(bs, comp, fp);
	}

	if (wrfl)
		fclose(fp);

	return ret;
}

/* End of file */
