/*
 * File:   bookie.c
 * Author: Alvin Difuntorum <alvinpd09@gmail.com>
 *
 *
 */

#include <stdio.h>
#include <stdlib.h>

#include "betstop.h"

int betstop_bookie_add(struct betstop *bs, struct bookie *bk)
{
	bk->bs = bs;
	INIT_LIST_HEAD(&bk->list);

	list_add_tail(&bk->list, &bs->bookies);

	return 0;
}

unsigned long bookie_team_get_id(struct bookie *bk, const char *name)
{
	struct team *tm, *t;
	struct betstop *bs = bk->bs;

	list_for_each_entry_safe(tm, t, &bk->teams, list) {
		if (!strcmp(name, tm->name))
			return tm->id;
	}

	fprintf(bs->ferr, "Unable to find team id: bookie=%s team=>%s<\n",
		bk->slug, name);

	return 0;
}

/* End of file */
