/*
 * File:   main.c
 * Author: Alvin Difuntorum <alvinpd09@gmail.com>
 *
 * BetStop application data loader
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "betstop.h"
#include "tables.h"

struct bs_loader {
	const char    *fname;
	const xmlChar *xpath;
	void (*parse)(struct betstop *bs, xmlDocPtr doc, xmlNodePtr cur);
};

static xmlDocPtr
bs_loader_getdoc(const char *docname)
{
	xmlDocPtr doc = xmlParseFile(docname);

	if (doc == NULL ) {
		bs_err("Document not parsed successfully: %s\n", docname);
		return NULL;
	}

	return doc;
}

static xmlXPathObjectPtr
bs_loader_getnodeset (xmlDocPtr doc, const xmlChar *xpath)
{
	xmlXPathObjectPtr  result;
	xmlXPathContextPtr context;

	context = xmlXPathNewContext(doc);
	if (context == NULL) {
		bs_err("Error in xmlXPathNewContext\n");
		return NULL;
	}

	result = xmlXPathEvalExpression(xpath, context);
	xmlXPathFreeContext(context);
	if (result == NULL) {
		bs_err("Error in xmlXPathEvalExpression\n");
		return NULL;
	}

	if(xmlXPathNodeSetIsEmpty(result->nodesetval)){
		xmlXPathFreeObject(result);
                bs_err("No result\n");
		return NULL;
	}

	return result;
}

static int table_load(struct bs_loader *bsl, struct betstop *bs)
{
	int i, ret = BS_ENONE;

	xmlDocPtr         doc = bs_loader_getdoc(bsl->fname);
	xmlNodeSetPtr     nodeset;
	xmlXPathObjectPtr result;

	if (!doc)
		return -BS_EEXIST;

	result = bs_loader_getnodeset(doc, bsl->xpath);
	if (result) {
		nodeset = result->nodesetval;

		for (i = 0; i < nodeset->nodeNr; i++)
			bsl->parse(bs, doc, nodeset->nodeTab[i]);

		xmlXPathFreeObject(result);
	}

	xmlFreeDoc(doc);

	return ret;
}

static int table_insert(struct betstop *bs, const char *query)
{
	int ret;
	char *err_msg;

	ret = sqlite3_exec(bs->db, query, NULL, 0, &err_msg);
	if (ret != SQLITE_OK) {
		bs_err("Insert query failed: %s\nQuery:\n%s\n",
			err_msg, query);
		sqlite3_free(err_msg);
	}

	return ret;
}

/*
 * XML data parsers
 */

static void parse_sports(struct betstop *bs, xmlDocPtr doc, xmlNodePtr cur)
{
	char    *s, query[1024];
	char    slug[32], name[32], name1[32], name2[32];
	xmlChar *p;

	/* Get the slug */
	p = xmlGetProp(cur, (const xmlChar *) "slug");
	if (!p)
		return;

	snprintf(slug, ARRAY_SIZE(slug), "%s", p);
	xmlFree(p);

	memset(name1, 0, ARRAY_SIZE(name1));
	memset(name2, 0, ARRAY_SIZE(name2));

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		s = NULL;
		if (!xmlStrcmp(cur->name, (const xmlChar *) "name"))
			s = name;
		else if (!xmlStrcmp(cur->name, (const xmlChar *) "alt_name1"))
			s = name1;
		else if (!xmlStrcmp(cur->name, (const xmlChar *) "alt_name2"))
			s = name2;

		if (s != NULL) {
			p = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
			if (p != NULL) {
				snprintf(s, 32, "%s", p);
				xmlFree(p);
			}
		}

		cur = cur->next;
	}

	sqlite3_snprintf(ARRAY_SIZE(query), query,
			"INSERT INTO sports(slug, name, name1, name2) " \
			"VALUES ('%q', '%q', '%q', '%q')\n",
			slug, name, name1, name2);
	table_insert(bs, query);
}

static void parse_bookies(struct betstop *bs, xmlDocPtr doc, xmlNodePtr cur)
{
	int  l;
	char *s, query[1024];
	char name[32], url[255];
	xmlChar *p;

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		s = NULL;
		if (!xmlStrcmp(cur->name, (const xmlChar *) "name")) {
			s = name;
			l = ARRAY_SIZE(name);
		} else if (!xmlStrcmp(cur->name, (const xmlChar *) "url")) {
			s = url;
			l = ARRAY_SIZE(url);
		}

		if (s != NULL) {
			p = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
			if (p != NULL) {
				snprintf(s, l, "%s", p);
				xmlFree(p);
			}
		}

		cur = cur->next;
	}

	sqlite3_snprintf(ARRAY_SIZE(query), query,
			"INSERT INTO bookies(name, url) VALUES('%q','%q')",
			name, url);
	table_insert(bs, query);
}

static void parse_leagues(struct betstop *bs, xmlDocPtr doc, xmlNodePtr cur)
{
	char    region[32], query[1024];
	xmlChar *p;
	unsigned long sport_id = 0;

	/* Get sport */
	p = xmlGetProp(cur, (const xmlChar *) "sport");
	if (p == NULL)
		return;

	sqlite3_snprintf(ARRAY_SIZE(query), query,
			"SELECT id FROM sports WHERE slug='%q'", p);
	betstop_db_query_id(bs, query, &sport_id);
	xmlFree(p);

	if (sport_id <= 0)
		return;

	/* Get region */
	p = xmlGetProp(cur, (const xmlChar *) "region");
	if (p == NULL)
		return;

	snprintf(region, ARRAY_SIZE(region), "%s", p);

	xmlFree(p);

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "name")) {
			p = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
			if (p != NULL) {
				sqlite3_snprintf(ARRAY_SIZE(query), query,
					"INSERT INTO leagues(sport_id, region, league) " \
					"VALUES (%d, '%q', '%q')",
					sport_id, region, p);
				table_insert(bs, query);
				xmlFree(p);
			}
		}

		cur = cur->next;
	}
}

static void parse_teams(struct betstop *bs, xmlDocPtr doc, xmlNodePtr cur)
{
	char region[32], league[64], query[1024];
	xmlChar *p;
	unsigned long sport_id = 0, league_id = 0;

	/* Get sport */
	p = xmlGetProp(cur, (const xmlChar *) "sport");
	if (p == NULL)
		return;

	sqlite3_snprintf(ARRAY_SIZE(query), query,
			"SELECT id FROM sports WHERE slug='%q'", p);
	betstop_db_query_id(bs, query, &sport_id);
	xmlFree(p);

	if (sport_id <= 0)
		return;

	/* Get region */
	p = xmlGetProp(cur, (const xmlChar *) "region");
	if (p == NULL)
		return;

	snprintf(region, ARRAY_SIZE(region), "%s", p);
	xmlFree(p);

	/* Get league*/
	p = xmlGetProp(cur, (const xmlChar *) "league");
	if (p == NULL)
		return;

	snprintf(league, ARRAY_SIZE(league), "%s", p);
	xmlFree(p);

	sqlite3_snprintf(ARRAY_SIZE(query), query,
			"SELECT id FROM leagues WHERE sport_id=%d AND " \
			"region='%q' AND league='%q'",
			sport_id, region, league);
	betstop_db_query_id(bs, query, &league_id);

	if (league_id <= 0)
		return;

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "name")) {
			p = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
			if (p != NULL) {
				sqlite3_snprintf(ARRAY_SIZE(query), query,
					"INSERT INTO teams(league_id, name) " \
					"VALUES (%d, '%q')",
					league_id, p);
				table_insert(bs, query);
				xmlFree(p);
			}
		}
		cur = cur->next;
	}
}

static void
parse_bookies_links(struct betstop *bs, xmlDocPtr doc, xmlNodePtr cur)
{
	char query[1024];
	xmlChar *p, *s;
	unsigned long bookie_id = 0, sport_id = 0;

	/* Get bookie */
	p = xmlGetProp(cur, (const xmlChar *) "bookie");
	if (p == NULL)
		return;

	snprintf(query, ARRAY_SIZE(query),
			"SELECT id FROM bookies WHERE name='%s'", p);
	betstop_db_query_id(bs, query, &bookie_id);
	xmlFree(p);

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "fetch")) {
			p = xmlGetProp(cur, (const xmlChar *) "sport");
			s = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);

			if (p && s) {
				sport_id = 0;
				sqlite3_snprintf(ARRAY_SIZE(query), query,
					"SELECT id FROM sports WHERE " \
					"slug='%q'", p);
				betstop_db_query_id(bs, query, &sport_id);
			}

			if (sport_id > 0) {
				sqlite3_snprintf(ARRAY_SIZE(query), query,
					"INSERT INTO bookies_links(bookie_id, " \
					"sport_id, fetch) VALUES (%d, %d, '%q')",
					bookie_id, sport_id, s);
				table_insert(bs, query);
			}

			if (p)
				xmlFree(p);
			if (s)
				xmlFree(s);
		}
		cur = cur->next;
	}
}

static void
parse_bookies_leagues(struct betstop *bs, xmlDocPtr doc, xmlNodePtr cur)
{
	char    region[32], league[32], query[1024];
	xmlChar *p, *s;
	unsigned long bookie_id = 0, league_id = 0, sport_id = 0;

	/* Get bookie */
	p = xmlGetProp(cur, (const xmlChar *) "bookie");
	if (p == NULL)
		return;

	sqlite3_snprintf(ARRAY_SIZE(query), query,
			"SELECT id FROM bookies WHERE name='%q'", p);
	betstop_db_query_id(bs, query, &bookie_id);
	xmlFree(p);

	if (bookie_id <= 0)
		return;

	/* Get sport */
	p = xmlGetProp(cur, (const xmlChar *) "sport");
	if (p == NULL)
		return;

	sqlite3_snprintf(ARRAY_SIZE(query), query,
			"SELECT id FROM sports WHERE slug='%q'", p);
	betstop_db_query_id(bs, query, &sport_id);
	xmlFree(p);

	/* Get region */
	p = xmlGetProp(cur, (const xmlChar *) "region");
	if (p == NULL)
		return;

	snprintf(region, ARRAY_SIZE(region), "%s", p);
	xmlFree(p);

	/* Get league */
	p = xmlGetProp(cur, (const xmlChar *) "league");
	if (p == NULL)
		return;

	/* Get league_id */
	snprintf(league, ARRAY_SIZE(league), "%s", p);
	xmlFree(p);

	sqlite3_snprintf(ARRAY_SIZE(query), query,
			"SELECT id FROM leagues WHERE " \
			"sport_id=%d AND region='%q' AND league='%q'",
			sport_id, region, league);

	betstop_db_query_id(bs, query, &league_id);
	if (league_id < 0)
		return;

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "url")) {
			p = xmlGetProp(cur, (const xmlChar *) "league");
			s = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);

			if (p && s) {
				sqlite3_snprintf(ARRAY_SIZE(query), query,
					"INSERT INTO bookies_leagues(bookie_id, "\
						"league_id, sport_id, url, league) "\
						"VALUES (%d, %d, %d, '%q', '%q')",
						bookie_id, league_id, sport_id,
						s, p);
				table_insert(bs, query);
			}

			if (p)
				xmlFree(p);
			if (s)
				xmlFree(s);
		}
		cur = cur->next;
	}
}

static void
parse_bookies_teams(struct betstop *bs, xmlDocPtr doc, xmlNodePtr cur)
{
	char    region[32], league[32], query[1024];
	xmlChar *p, *s;
	unsigned long bookie_id = 0, league_id = 0, sport_id = 0, team_id = 0;

	/* Get bookie */
	p = xmlGetProp(cur, (const xmlChar *) "bookie");
	if (p == NULL)
		return;

	sqlite3_snprintf(ARRAY_SIZE(query), query,
			"SELECT id FROM bookies WHERE name='%q'", p);
	betstop_db_query_id(bs, query, &bookie_id);
	xmlFree(p);

	if (bookie_id <= 0)
		return;

	/* Get sport */
	p = xmlGetProp(cur, (const xmlChar *) "sport");
	if (p == NULL)
		return;

	sqlite3_snprintf(ARRAY_SIZE(query), query,
			"SELECT id FROM sports WHERE slug='%q'", p);
	betstop_db_query_id(bs, query, &sport_id);
	xmlFree(p);

	/* Get region */
	p = xmlGetProp(cur, (const xmlChar *) "region");
	if (p == NULL)
		return;

	snprintf(region, ARRAY_SIZE(region), "%s", p);
	xmlFree(p);

	/* Get league */
	p = xmlGetProp(cur, (const xmlChar *) "league");
	if (p == NULL)
		return;

	/* Get league_id */
	snprintf(league, ARRAY_SIZE(league), "%s", p);
	xmlFree(p);

	sqlite3_snprintf(ARRAY_SIZE(query), query,
			"SELECT id FROM leagues WHERE " \
			"sport_id=%d AND region='%q' AND league='%q'",
			sport_id, region, league);

	betstop_db_query_id(bs, query, &league_id);
	if (league_id < 0)
		return;

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (!xmlStrcmp(cur->name, (const xmlChar *) "name")) {
			team_id = 0;
			p = xmlGetProp(cur, (const xmlChar *) "team");
			s = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
			if (p && s) {
				sqlite3_snprintf(ARRAY_SIZE(query), query,
					"SELECT id FROM teams WHERE " \
					"league_id=%d AND name='%q'",
					league_id, p);
				betstop_db_query_id(bs, query, &team_id);
			}

			if (team_id > 0) {
				sqlite3_snprintf(ARRAY_SIZE(query), query,
					"INSERT INTO bookies_teams(bookie_id, "\
						"team_id, league_id, name) VALUES "\
						"(%d, %d, %d, '%q')",
						bookie_id, team_id, league_id, s);
				table_insert(bs, query);
			}

			if (p)
				xmlFree(p);
			if (s)
				xmlFree(s);
		}
		cur = cur->next;
	}
}

/*
 * Static data
 */

#if defined(BS_LOADER_TABLE_DROP)
static const char *loader_drop[] = {
	BS_LOADER_TABLE_DROP_MATCHES,
	BS_LOADER_TABLE_DROP_BOOKIES_TEAMS,
	BS_LOADER_TABLE_DROP_BOOKIES_LINKS,
	BS_LOADER_TABLE_DROP_BOOKIES_LEAGUES,
	BS_LOADER_TABLE_DROP_TEAMS,
	BS_LOADER_TABLE_DROP_LEAGUES,
	BS_LOADER_TABLE_DROP_BOOKIES,
	BS_LOADER_TABLE_DROP_SPORTS,
};
#endif
static const char *loader_create[] = {
	BS_LOADER_TABLE_CREATE_SPORTS,
	BS_LOADER_TABLE_CREATE_BOOKIES,
	BS_LOADER_TABLE_CREATE_LEAGUES,
	BS_LOADER_TABLE_CREATE_TEAMS,
	BS_LOADER_TABLE_CREATE_BOOKIES_LEAGUES,
	BS_LOADER_TABLE_CREATE_BOOKIES_LINKS,
	BS_LOADER_TABLE_CREATE_BOOKIES_TEAMS,
	BS_LOADER_TABLE_CREATE_MATCHES,
};

static struct bs_loader loader_bsl[] = {
	{
		.fname = "xml/sports.xml",
		.xpath = (const xmlChar *) "//sports/sport",
		.parse = parse_sports,
	},
	{
		.fname = "xml/bookies.xml",
		.xpath = (const xmlChar *) "//bookies/bookie",
		.parse = parse_bookies,
	},
	{
		.fname = "xml/bookies_links.xml",
		.xpath = (const xmlChar *) "//bookies_links/bl",
		.parse = parse_bookies_links,
	},
};

static const char *loader_leagues[] = {
	"xml/leagues-handball.xml",
	"xml/leagues-soccer.xml",
};

static const char *loader_teams[] = {
	"xml/teams-handball.xml",
	"xml/teams-soccer.xml",
};

static const char *loader_bookies_leagues[] = {
	"xml/bookies_leagues-handball.xml",
	"xml/bookies_leagues-soccer.xml",
};

static const char *loader_bookies_teams[] = {
	"xml/bookies_teams-handball.xml",
	"xml/bookies_teams-soccer.xml",
};

int betstop_load_data(struct betstop *bs)
{
	int  i, ret = BS_ENONE;
	char *err_msg;
	struct bs_loader bsl;


#if defined(BS_LOADER_TABLE_DROP)
	/* Drop all the tables */
	for (i = 0; i < ARRAY_SIZE(loader_drop); i++) {
		ret = sqlite3_exec(bs->db, loader_drop[i], NULL, 0, &err_msg);
		if (ret != SQLITE_OK) {
			bs_err("Drop query failed: %s\nQuery:\n%s\n",
				err_msg, loader_drop[i]);
			sqlite3_free(err_msg);
		}
	}
#endif

	/* Create all the tables */
	for (i = 0; i < ARRAY_SIZE(loader_create); i++) {
		ret = sqlite3_exec(bs->db, loader_create[i], NULL, 0, &err_msg);
		if (ret != SQLITE_OK) {
			bs_err("Create query failed: %s\nQuery:\n%s\n",
				err_msg, loader_create[i]);
			sqlite3_free(err_msg);
		}
	}

	/* Load initial table contents */
	for (i = 0; i < ARRAY_SIZE(loader_bsl); i++)
		table_load(&loader_bsl[i], bs);

	/* Load leagues */
	bsl.parse = parse_leagues;
	bsl.xpath = (const xmlChar *) "//leagues/league";
	for (i = 0; i < ARRAY_SIZE(loader_leagues); i++) {
		bsl.fname = loader_leagues[i];
		table_load(&bsl, bs);
	}

	/* Load teams */
	bsl.parse = parse_teams;
	bsl.xpath = (const xmlChar *) "//teams/team";
	for (i = 0; i < ARRAY_SIZE(loader_teams); i++) {
		bsl.fname = loader_teams[i];
		table_load(&bsl, bs);
	}

	/* Load bookies_leagues */
	bsl.parse = parse_bookies_leagues;
	bsl.xpath = (const xmlChar *) "//bookies_leagues/bl";
	for (i = 0; i < ARRAY_SIZE(loader_bookies_leagues); i++) {
		bsl.fname = loader_bookies_leagues[i];
		table_load(&bsl, bs);
	}

	/* Load bookies_teams */
	bsl.parse = parse_bookies_teams;
	bsl.xpath = (const xmlChar *) "//bookies_teams/bt";
	for (i = 0; i < ARRAY_SIZE(loader_bookies_teams); i++) {
		bsl.fname = loader_bookies_teams[i];
		table_load(&bsl, bs);
	}

	return ret;
}

/* End of file */
