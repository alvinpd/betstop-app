/*
 * File:   tables.h
 * Author: Alvin Difuntorum <alvinpd09@gmail.com>
 *
 *
 */

#ifndef __LOADER_TABLES_H__
#define	__LOADER_TABLES_H__

#define BS_LOADER_TABLE_DROP

#if defined(BS_LOADER_TABLE_DROP)

#define BS_LOADER_TABLE_DROP_SPORTS		\
	"DROP TABLE IF EXISTS `sports`"

#define BS_LOADER_TABLE_DROP_BOOKIES		\
	"DROP TABLE IF EXISTS `bookies`"

#define BS_LOADER_TABLE_DROP_LEAGUES		\
	"DROP TABLE IF EXISTS `leagues`"

#define BS_LOADER_TABLE_DROP_TEAMS		\
	"DROP TABLE IF EXISTS `teams`"

#define BS_LOADER_TABLE_DROP_BOOKIES_LEAGUES	\
	"DROP TABLE IF EXISTS `bookies_leagues`"

#define BS_LOADER_TABLE_DROP_BOOKIES_LINKS	\
	"DROP TABLE IF EXISTS `bookies_links`"

#define BS_LOADER_TABLE_DROP_BOOKIES_TEAMS	\
	"DROP TABLE IF EXISTS `bookies_teams`"

#define BS_LOADER_TABLE_DROP_MATCHES		\
	"DROP TABLE IF EXISTS `matches`"

#endif /* BS_LOADER_TABLE_DROP */

#define BS_LOADER_TABLE_CREATE_SPORTS		\
	"CREATE TABLE IF NOT EXISTS `sports` (\n" \
	"    `id`         INTEGER PRIMARY KEY AUTOINCREMENT,\n" \
	"    `slug`       VARCHAR(32) UNIQUE NOT NULL DEFAULT '',\n" \
	"    `name`       VARCHAR(32) UNIQUE NOT NULL DEFAULT '',\n" \
	"    `name1`      VARCHAR(32),\n" \
	"    `name2`      VARCHAR(32))"

#define BS_LOADER_TABLE_CREATE_BOOKIES		\
	"CREATE TABLE IF NOT EXISTS `bookies` (\n" \
	"    `id`         INTEGER PRIMARY KEY AUTOINCREMENT,\n" \
	"    `name`       VARCHAR(32) UNIQUE NOT NULL DEFAULT '',\n" \
	"    `url`        VARCHAR(255) NOT NULL DEFAULT '')"

#define BS_LOADER_TABLE_CREATE_LEAGUES		\
	"CREATE TABLE IF NOT EXISTS `leagues` (\n" \
	"    `id`         INTEGER PRIMARY KEY AUTOINCREMENT,\n" \
	"    `sport_id`   INTEGER NOT NULL DEFAULT 0,\n" \
	"    `region`     VARCHAR(32) NOT NULL DEFAULT '',\n" \
	"    `league`     VARCHAR(64) NOT NULL DEFAULT '',\n" \
	"\n" \
	"    CONSTRAINT `fk_sport_id` FOREIGN KEY (`sport_id`)\n" \
	"        REFERENCES `sports` (`id`)\n" \
	"        ON DELETE NO ACTION ON UPDATE NO ACTION)"

#define BS_LOADER_TABLE_CREATE_TEAMS		\
	"CREATE TABLE IF NOT EXISTS `teams` (\n" \
	"    `id`         INTEGER PRIMARY KEY AUTOINCREMENT,\n" \
	"    `league_id`  INTEGER NOT NULL DEFAULT 0,\n" \
	"    `name`       VARCHAR(64) NOT NULL DEFAULT '',\n" \
	"\n" \
	"    CONSTRAINT `fk_league_id` FOREIGN KEY (`league_id`)\n" \
	"        REFERENCES `leagues`(`id`)\n" \
	"        ON DELETE NO ACTION ON UPDATE NO ACTION)"

#define BS_LOADER_TABLE_CREATE_BOOKIES_LEAGUES	\
	"CREATE  TABLE IF NOT EXISTS `bookies_leagues` (\n" \
	"    `id`         INTEGER PRIMARY KEY AUTOINCREMENT ,\n" \
	"    `bookie_id`  INTEGER NOT NULL DEFAULT 0,\n" \
	"    `league_id`  INTEGER NOT NULL DEFAULT 0,\n" \
	"    `sport_id`   INTEGER NOT NULL DEFAULT 0,\n" \
	"    `url`        VARCHAR(512) NOT NULL DEFAULT '',\n" \
	"    `league`     VARCHAR(64) NOT NULL DEFAULT '' ,\n" \
	"\n" \
	"    CONSTRAINT `fk_bookie_id` FOREIGN KEY (`bookie_id`)\n" \
	"        REFERENCES `bookies`(`id`)\n"\
	"        ON DELETE NO ACTION ON UPDATE NO ACTION,\n" \
	"    CONSTRAINT `fk_league_id` FOREIGN KEY (`league_id`)\n" \
	"        REFERENCES `leagues`(`id`)\n" \
	"        ON DELETE NO ACTION ON UPDATE NO ACTION,\n" \
	"    CONSTRAINT `fk_sport_id` FOREIGN KEY (`sport_id`)\n" \
	"        REFERENCES `sports`(`id`)\n" \
	"        ON DELETE NO ACTION ON UPDATE NO ACTION)"

#define BS_LOADER_TABLE_CREATE_BOOKIES_LINKS	\
	"CREATE  TABLE IF NOT EXISTS `bookies_links` (\n" \
	"    `id`         INTEGER PRIMARY KEY AUTOINCREMENT,\n" \
	"    `bookie_id`  INT UNSIGNED NOT NULL DEFAULT 0,\n" \
	"    `sport_id`   INT UNSIGNED NOT NULL DEFAULT 0,\n" \
	"    `fetch`      VARCHAR(512) NOT NULL DEFAULT '',\n" \
	"\n" \
	"    CONSTRAINT `fk_bookie_id` FOREIGN KEY (`bookie_id`)\n" \
	"        REFERENCES `bookies`(`id`)\n" \
	"        ON DELETE NO ACTION ON UPDATE NO ACTION,\n" \
	"    CONSTRAINT `fk_sport_id` FOREIGN KEY (`sport_id`)\n" \
	"        REFERENCES `sports`(`id`)\n" \
	"        ON DELETE NO ACTION ON UPDATE NO ACTION)"

#define BS_LOADER_TABLE_CREATE_BOOKIES_TEAMS	\
	"CREATE  TABLE IF NOT EXISTS `bookies_teams` (\n" \
	"    `id`         INTEGER PRIMARY KEY AUTOINCREMENT,\n" \
	"    `bookie_id`  INTEGER NOT NULL DEFAULT 0,\n" \
	"    `team_id`    INTEGER NOT NULL DEFAULT 0,\n" \
	"    `league_id`  INTEGER NOT NULL DEFAULT 0,\n" \
	"    `name`       VARCHAR(64) NOT NULL DEFAULT '',\n" \
	"\n" \
	"    CONSTRAINT `fk_bookie_id` FOREIGN KEY (`bookie_id`)\n" \
	"        REFERENCES `bookies` (`id`)\n" \
	"        ON DELETE NO ACTION ON UPDATE NO ACTION,\n" \
	"    CONSTRAINT `fk_team_id` FOREIGN KEY (`team_id`)\n" \
	"        REFERENCES `teams` (`id`)\n" \
	"        ON DELETE NO ACTION ON UPDATE NO ACTION,\n" \
	"    CONSTRAINT `fk_league_id` FOREIGN KEY (`league_id`)\n" \
	"        REFERENCES `leagues` (`id`)\n" \
	"        ON DELETE NO ACTION ON UPDATE NO ACTION)\n"

/*
 * Table `matches`:
 *
 * This table defines the various matches provided by the bookies and the
 * odds on various markets.
 *     ml1, ml2: Moneyline bets
 *     odd1, odd2, odd3: 3-Way bets
 *     ah1, ah2: Asian Handicap bets
 *     dc1, dc2: Double Chance bets
 *     eh1, eh2: European Handicap bets
 *     ou1, ou2: Over/Under bets
 */
#define BS_LOADER_TABLE_CREATE_MATCHES		\
	"CREATE  TABLE IF NOT EXISTS `matches` (\n" \
	"    `id`         INTEGER PRIMARY KEY AUTOINCREMENT,\n" \
	"    `bookie_id`  INTEGER NOT NULL DEFAULT 0,\n" \
	"    `league_id`  INTEGER NOT NULL DEFAULT 0,\n" \
	"    `team1_id`   INTEGER NOT NULL DEFAULT 0,\n" \
	"    `team2_id`   INTEGER NOT NULL DEFAULT 0,\n" \
	"    `sched`      INTEGER NOT NULL DEFAULT 0,\n" \
	"    `ml1`        DOUBLE NOT NULL DEFAULT 0,\n" \
	"    `ml2`        DOUBLE NOT NULL DEFAULT 0,\n" \
	"    `odd1`       DOUBLE NOT NULL DEFAULT 0,\n" \
	"    `odd2`       DOUBLE NOT NULL DEFAULT 0,\n" \
	"    `odd3`       DOUBLE NOT NULL DEFAULT 0,\n" \
	"    `ah1`        DOUBLE NOT NULL DEFAULT 0,\n" \
	"    `ah2`        DOUBLE NOT NULL DEFAULT 0,\n" \
	"    `dc1`        DOUBLE NOT NULL DEFAULT 0,\n" \
	"    `dc2`        DOUBLE NOT NULL DEFAULT 0,\n" \
	"    `eh1`        DOUBLE NOT NULL DEFAULT 0,\n" \
	"    `eh2`        DOUBLE NOT NULL DEFAULT 0,\n" \
	"    `ou1`        DOUBLE NOT NULL DEFAULT 0,\n" \
	"    `ou2`        DOUBLE NOT NULL DEFAULT 0,\n" \
	"\n" \
	"    CONSTRAINT `fk_bookie_id` FOREIGN KEY (`bookie_id`)\n" \
	"        REFERENCES `bookies` (`id`)\n" \
	"        ON DELETE NO ACTION ON UPDATE NO ACTION,\n" \
	"    CONSTRAINT `fk_league_id` FOREIGN KEY (`league_id`)\n" \
	"        REFERENCES `leagues` (`id`)\n" \
	"        ON DELETE NO ACTION ON UPDATE NO ACTION,\n" \
	"    CONSTRAINT `fk_team1_id` FOREIGN KEY (`team1_id`)\n" \
	"        REFERENCES `teams` (`id`)\n" \
	"        ON DELETE NO ACTION ON UPDATE NO ACTION,\n" \
	"    CONSTRAINT `fk_team2_id` FOREIGN KEY (`team2_id`)\n" \
	"        REFERENCES `teams` (`id`)\n" \
	"        ON DELETE NO ACTION ON UPDATE NO ACTION)"

#endif	/* __LOADER_TABLES_H__ */

